package com.example.biz.order.el.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.api.entity.order.OrderInfo;
import com.example.biz.order.el.dao.OrderMapper;
import com.example.biz.order.el.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Administrator
 */
@Service
@Slf4j
@AllArgsConstructor
public class OrderServiceImpl extends ServiceImpl<OrderMapper, OrderInfo> implements OrderService {

    private final OrderMapper orderMapper;


    @Override
    public List<OrderInfo> getOrderlist() {
        final List<OrderInfo> orderEntities = baseMapper.getOrderlist();
        return orderEntities;
    }

    @Override
    public OrderInfo insertOrderEntity(OrderInfo orderEntity) {
        log.info("进入订单微服务====" + JSONUtil.toJsonStr(orderEntity));
        BigDecimal.ZERO.divide(BigDecimal.ZERO);
        boolean insert = orderEntity.insert();
        return insert ? orderEntity : null;
    }
}
