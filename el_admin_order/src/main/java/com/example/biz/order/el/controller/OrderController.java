package com.example.biz.order.el.controller;


import cn.hutool.core.collection.CollUtil;
import com.example.api.entity.order.OrderInfo;
import com.example.biz.order.el.service.OrderService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lr
 * @since 2021-04-17
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("/list")
    public List<OrderInfo> list() {
        String a = null;

        List<OrderInfo> list = orderService.getOrderlist();
        System.out.println(a.toString());
        return list;
    }

    @GetMapping("/getRedis")
    public List<OrderInfo> getRedis() {
        Object get_redis = redisTemplate.opsForValue().get("get_redis");
        List<OrderInfo> list = orderService.getOrderlist();
        if (null == get_redis) {
            if (CollUtil.isNotEmpty(list)) {
                redisTemplate.opsForValue().set("get_redis", list);
            } else {
                //是否需要null值存入
            }
        }
        return list;
    }

    /**
     * fallbackMethod是发生错误时调用的方法
     *
     * @param orderEntity
     * @return
     * @GlobalTransactional(rollbackFor = Exception.class)
     */
    @PostMapping("/insertOrderEntity")
    @HystrixCommand(fallbackMethod = "hystrixSimpleTest")
    public OrderInfo insertOrderEntity(@RequestBody OrderInfo orderEntity) {
        OrderInfo entity = orderService.insertOrderEntity(orderEntity);
        return entity;
    }

    private OrderInfo hystrixSimpleTest(OrderInfo orderEntity) {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderId("22");
        orderInfo.setOrderType("22");
        return orderInfo;
    }

}

