package com.example.biz.order.el.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.entity.order.OrderInfo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lr
 * @since 2021-04-17
 */
public interface OrderService extends IService<OrderInfo> {

    List<OrderInfo> getOrderlist();


    OrderInfo insertOrderEntity(OrderInfo orderEntity);

}
