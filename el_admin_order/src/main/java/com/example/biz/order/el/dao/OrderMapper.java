package com.example.biz.order.el.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.api.entity.order.OrderInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lr
 * @since 2021-04-17
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderInfo> {

    List<OrderInfo> getOrderlist();
}
