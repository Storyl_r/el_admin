package com.example.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.example.annotations.JsonToObject;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author json转换
 * @since 2023/8/22 9:32
 */
@Slf4j
public class JsonUtil {

    /**
     * 将Json转换为对象
     *
     * @param object
     * @param tClass
     * @return
     */
    public static Object jsonToObject(Object object, Class tClass) {
        Map<String, ? extends Class<?>> collect = Arrays.stream(tClass.getDeclaredFields()).collect(Collectors.toMap(x -> x.getName(), y -> y.getType()));
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (null != field.getAnnotation(JsonToObject.class)) {
                field.setAccessible(true);
                try {
                    String jsonFieldValue = (String) field.get(object);
                    if (StrUtil.isNotBlank(jsonFieldValue)) {
                        Class<?> aClass = collect.get(field.getName());
                        Object javaObject;
                        boolean jsonArray = JSONUtil.isJsonArray(jsonFieldValue);
                        if (jsonArray) {
                            javaObject = JSONUtil.toList(jsonFieldValue, aClass);
                        } else {
                            javaObject = JSONUtil.toBean(jsonFieldValue, aClass);
                        }
                        field.set(object, javaObject);
                    }
                } catch (IllegalAccessException e) {
                    log.error("jsonToObject异常:{},数据{}", e.getMessage(), object);
                }
            }
        }
        return object;
    }
}
