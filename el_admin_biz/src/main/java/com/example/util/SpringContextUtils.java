package com.example.util;

import cn.hutool.extra.spring.SpringUtil;

/**
 * <p>描述：spring上下文工具类 </p>
 *
 * @author huan
 */
public class SpringContextUtils {

    /**
     * 获取指定的Bean
     *
     * @param beanName
     * @param clz
     * @param <T>
     * @return
     */
    public static <T> T getBean(String beanName, Class<T> clz) {
        return SpringUtil.getBean(beanName, clz);
    }
}

