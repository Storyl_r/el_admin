package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.api.entity.biz.Account;
import com.example.mapper.AccountMapper;
import com.example.service.AccountService;
import com.example.service.ThreadPoolExecutorService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author lr
 * @description 账号
 * @date 2022-03-18
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

    private final AccountMapper accountMapper;


    @Override
    public Object insert(Account account) {
        // valid
        if (account == null) {
            return null;
        }
        accountMapper.insert(account);
        return account;
    }


    @Override
    public Object delete(int id) {
        int ret = accountMapper.delete(id);
        return null;
    }


    @Override
    public Account load(int id) {
        return accountMapper.load(id);
    }


    @Override
    public Map<String, Object> pageList(int offset, int pagesize) {

        List<Account> pageList = accountMapper.pageList(offset, pagesize);
        int totalCount = accountMapper.pageListCount(offset, pagesize);

        // result
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("pageList", pageList);
        result.put("totalCount", totalCount);

        return result;
    }

    /**
     * 在初始化完成后执行
     */
    @PostConstruct
    public void init() {
        System.out.println("ccc");
    }
}