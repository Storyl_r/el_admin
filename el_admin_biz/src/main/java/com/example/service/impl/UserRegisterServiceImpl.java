package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.api.entity.biz.SysUserRegister;
import com.example.mapper.SysUserRegisterMapper;
import com.example.service.UserRegisterService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户注册信息 服务实现类
 * </p>
 *
 * @author lr
 * @since 2022-03-26
 */
@Service
public class UserRegisterServiceImpl extends ServiceImpl<SysUserRegisterMapper, SysUserRegister> implements UserRegisterService {

}
