package com.example.service;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Author Story
 * @Date 2022/3/23 19:58
 * @Version 1.0
 */
public interface ThreadPoolExecutorService {

    /**
     * 使用线程池执行任务 第一种是 CPU 密集型任务，比如加密、解密、压缩、计算等一系列需要大量耗费 CPU 资源的任务 第二种任务是耗时 IO 型，比如数据库、文件的读写，网络通信等任务，这种任务的特点是并不会特别消耗 CPU 资源，但是 IO 操作很耗时，总体会占用比较多的时间
     *
     * @param
     * @param type 0 io密集流 1cpu密集型
     * @return
     */
    ThreadPoolExecutor executeTaskThread(String factoryName, String type);

    /**
     * 使用线程池执行任务 第一种是 CPU 密集型任务，比如加密、解密、压缩、计算等一系列需要大量耗费 CPU 资源的任务 第二种任务是耗时 IO 型，比如数据库、文件的读写，网络通信等任务，这种任务的特点是并不会特别消耗 CPU 资源，但是 IO 操作很耗时，总体会占用比较多的时间
     *
     * @param
     * @return
     */
    ScheduledExecutorService executeScheduledTaskThread(Integer coor);
}
