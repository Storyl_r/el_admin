package com.example.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.entity.biz.User;
import com.example.api.vo.biz.UserVo;
import org.springframework.context.annotation.Scope;

/**
 * @Author Story
 * @Date 2022/3/19 23:20
 * @Version 1.0
 */
public interface UserService extends IService<User> {
    /**
     * 根据主键 id 查询
     */
    UserVo load(String id);

    /**
     * 批量插入用户
     *
     * @return
     */
    UserVo addUser(UserVo userVo);

    /**
     * 登陆
     *
     * @param userName
     * @param password
     */
    User login(String userName, String password);
}
