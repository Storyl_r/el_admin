package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.entity.biz.SysUserRegister;

/**
 * <p>
 * 用户注册信息 服务类
 * </p>
 *
 * @author lr
 * @since 2022-03-26
 */
public interface UserRegisterService extends IService<SysUserRegister> {

}
