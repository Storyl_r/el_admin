package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.entity.biz.Account;

import java.util.Map;

/**
 * @author lr
 * @description 账号
 * @date 2022-03-18
 */
public interface AccountService extends IService<Account> {

    /**
     * 新增
     */
    public Object insert(Account account);

    /**
     * 删除
     */
    public Object delete(int id);


    /**
     * 根据主键 id 查询
     */
    public Account load(int id);

    /**
     * 分页查询
     */
    public Map<String, Object> pageList(int offset, int pagesize);


}