package com.example.feign.order;

import com.example.api.entity.order.OrderInfo;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class FallBackFactory  implements FallbackFactory<OrderService> {

    @Override
    public OrderService create(Throwable throwable) {
        return new OrderService() {
            @Override
            public List<OrderInfo> getOrderlist() {
                log.warn("进入order降级方法:getOrderlist");
                return null;
            }

            @Override
            public OrderInfo insert(OrderInfo orderEntity) {
                log.warn("进入order降级方法：insert");
                return null;
            }
        };
    }
}
