package com.example.feign.order;

import com.example.api.entity.order.OrderInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name = "el-admin-order",fallbackFactory = FallBackFactory.class)
public interface OrderService {

    @GetMapping("/order/list")
    List<OrderInfo> getOrderlist();

    @PostMapping(value = "/order/insertOrderEntity", consumes = "application/json")
    OrderInfo insert(OrderInfo orderEntity);
}
