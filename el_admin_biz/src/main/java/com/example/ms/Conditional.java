package com.example.ms;

import org.apache.catalina.startup.Tomcat;
import org.apache.coyote.UpgradeProtocol;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.SearchStrategy;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.servlet.Servlet;

/**
 * @author 条件匹配
 * @since 2023/4/7 10:00
 * 先解析用户的定义的Bean，也就是解析用户定义的配置类（包含了扫描和@Bean的解析）
 * 再解析SpringBoot中的自动配置类<br/>
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass({Servlet.class, Tomcat.class, UpgradeProtocol.class})
@ConditionalOnMissingBean(value = ServletWebServerFactory.class, search =
        SearchStrategy.CURRENT)

public class Conditional {

}


