//package com.example.ms.sping;
//
//import com.example.easyexcel.UserInfoModel;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
///**
// * @author 自定义密码验证规则
// * @since 2023/8/11 17:29
// */
//@Component
//public class MyAuthenticationProvider implements AuthenticationProvider {
//
//    @Autowired
//    BCryptPasswordEncoder passwordEncoder;
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        String username = authentication.getName();     //表单提交的用户名
//        String presentedPassword = (String) authentication.getCredentials();     //表单提交的密码
//        // 根据用户名获取用户信息
//        UserInfoModel sysUser = new UserInfoModel();
//        if (null!=sysUser) {
//            throw new BadCredentialsException("用户名不存在");
//        } else {
//            List<GrantedAuthority> userAuthority = getUserAuthority(sysUser.getId());
//            UserInfoModel userDeatils = new UserInfoModel(getUserAuthority(sysUser.getId()));
//
//            if (authentication.getCredentials() == null) {
//                throw new BadCredentialsException("凭证为空");
//            } else if (!passwordEncoder.matches(presentedPassword, sysUser.getPassword())) {
//                System.out.println("encodedPassword:" + presentedPassword);
//                System.out.println("password:" + sysUser.getPassword());
//                throw new BadCredentialsException("密码错误");
//            } else {
//                UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(userDeatils, authentication.getCredentials(), userAuthority);
//                result.setDetails(authentication.getDetails());
//                return result;
//            }
//        }
//
//    }
//
//    @Override
//    public boolean supports(Class<?> aClass) {
//        return false;
//    }
//
//    //获取用户权限
//    public List<GrantedAuthority> getUserAuthority(String userId) {
//        // 角色(ROLE_admin)、菜单操作权限 sys:user:list
//        // ROLE_admin,ROLE_normal,sys:user:list,....
//        String authority = "";
//        return AuthorityUtils.commaSeparatedStringToAuthorityList(authority);
//    }
//
//    public static void main(String[] args) {
//        String pass = "111111";
//        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
//        String hashPass = bcryptPasswordEncoder.encode(pass);
//        System.out.println(hashPass);
//
//        boolean f = bcryptPasswordEncoder.matches("111111", hashPass);
//        System.out.println(f);
//    }
//}
