package com.example.ms;

import org.springframework.context.annotation.ComponentScan;

/**
 * 扫描
 *
 * @author lr
 * @create 2023/1/1
 */
@ComponentScan("com.example.ms")
public class AppConfig {

}