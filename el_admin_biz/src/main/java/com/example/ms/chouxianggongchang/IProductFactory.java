package com.example.ms.chouxianggongchang;

/**
 * 工厂实现类
 */
public interface IProductFactory {
    /**
     * 手机
     *
     * @return
     */
    IPhoneProduct getPhone();

    /**
     * 路由
     *
     * @return
     */
    IRouterProduct getRouter();
}
