package com.example.ms.chouxianggongchang;

public class HuaweiFactory implements IProductFactory {
    @Override
    public IPhoneProduct getPhone() {
        return new HwPhone();
    }

    @Override
    public IRouterProduct getRouter() {
        return new HwRouter();
    }
}
