package com.example.ms.chouxianggongchang;

/**
 * 根据不同的用户做不同的事情（抽象工厂）
 */
public interface IPhoneProduct {
    //开机
    void start();
    //关机
    void shutdown();
}

