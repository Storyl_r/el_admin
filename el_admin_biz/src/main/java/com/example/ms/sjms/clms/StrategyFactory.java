package com.example.ms.sjms.clms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * 工厂类
 *
 * @author lr
 * @create 2023/2/2
 */
@Component
@ConditionalOnBean(ConcreteStrategy1.class)
public class StrategyFactory {

    //Spring会自动将Strategy接口的实现类注入到这个Map中，key为bean id，value值则为对应的策略实现类

    @Autowired
    private Map<String, Strategy> strategyMap;

    public Strategy getBy(String strategyName) {
        return strategyMap.get(strategyName);
    }
    @PostConstruct
    public  void  aa(){
        System.out.println("初始化完成**********");
    }
}