package com.example.ms.sjms.clms;

/**
 * 抽象策略角色
 *
 * @author lr
 * @create 2023/2/2
 */
public interface Strategy {

    /**
     * 策略模式的运算法则
     */
    void doWhat();
}