package com.example.ms.sjms.clms;

import com.example.forest.ServiceUrl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

/**
 * @author lr
 * @create 2023/2/2
 */
@Component
@ConditionalOnBean(ServiceUrl.class)
public class ConcreteStrategy1 implements Strategy {
    //具体策略角色

    @Override
    public void doWhat() {
        System.out.println("具体策略1的运算法则...");
    }

    @Override
    public String toString() {
        return "具体策略1";
    }
}