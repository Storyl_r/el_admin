package com.example.ms.sjms.clms;

import org.springframework.stereotype.Component;

/**
 * @author lr
 * @create 2023/2/2
 */
@Component
public class ConcreteStrategy2 implements Strategy {
    //具体策略角色

    @Override
    public void doWhat() {
        System.out.println("具体策略2的运算法则...");
    }

    @Override
    public String toString() {
        return "具体策略2";
    }
}