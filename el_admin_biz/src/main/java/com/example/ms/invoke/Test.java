package com.example.ms.invoke;

/**
 * @author lr
 * @create 2023/2/3
 */
public class Test {

    public static void main(String[] args) {
        HelloService hello = ProxyUtils.createProxy(new HelloImpl());
        hello.sayHello("xxxxx");

    }
}