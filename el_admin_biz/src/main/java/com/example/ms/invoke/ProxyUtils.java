package com.example.ms.invoke;

import java.lang.reflect.Proxy;

/**
 * @author lr
 * @create 2023/2/3
 */
public class ProxyUtils {

    /**
     * 创建代理
     *
     * @param hello
     * @return
     */
    public static HelloService createProxy(HelloService hello) {
        HelloService hello1 = (HelloService) Proxy.newProxyInstance(ProxyUtils.class.getClassLoader(), new Class[]{HelloService.class}, (proxy, method, args) -> {
            if ("sayHello".equals(method.getName())) {
                System.out.println(method.getName() + "方法进行代理");
            }
            return method.invoke(hello, args);

        });
        return hello1;
    }


}