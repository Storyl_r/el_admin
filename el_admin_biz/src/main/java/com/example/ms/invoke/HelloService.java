package com.example.ms.invoke;

/**
 * @author lr
 * @create 2023/2/3
 */
public interface HelloService {

    void sayHello(String message);
}