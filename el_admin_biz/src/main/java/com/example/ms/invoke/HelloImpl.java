package com.example.ms.invoke;

/**
 * @author lr
 * @create 2023/2/3
 */
public class HelloImpl implements HelloService {

    @Override
    public void sayHello(String message) {
        System.out.println(message + "您好");
    }
}