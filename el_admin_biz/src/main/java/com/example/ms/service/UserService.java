//package com.example.ms.service;
//
//import org.apache.naming.factory.BeanFactory;
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.FactoryBean;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.config.BeanPostProcessor;
//import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
//import org.springframework.beans.factory.support.BeanDefinitionRegistry;
//import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
//import org.springframework.stereotype.Component;
//
///**
// * 用户
// *
// * @author lr
// * @create 2023/1/1
// */
//@Component
//public class UserService extends BeanFactory implements BeanDefinitionRegistryPostProcessor, BeanPostProcessor {
//
//    @Autowired
//    private  OrderService orderService;
//
//    public void test() {
//        System.out.println("userService");
//    }
//
//    /**
//     * Modify the application context's internal bean definition registry after its standard initialization. All regular bean definitions will have been loaded, but no beans will have been instantiated yet. This allows for adding further
//     * bean definitions before the next post-processing phase kicks in.
//     *
//     * @param registry the bean definition registry used by the application context
//     * @throws BeansException in case of errors
//     */
//    @Override
//    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
//        System.out.println("注册前：postProcessBeanDefinitionRegistry");
//    }
//
//    /**
//     * Modify the application context's internal bean factory after its standard initialization. All bean definitions will have been loaded, but no beans will have been instantiated yet. This allows for overriding or adding properties even
//     * to eager-initializing beans.
//     *
//     * @param beanFactory the bean factory used by the application context
//     * @throws BeansException in case of errors
//     */
//    @Override
//    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
//        System.out.println("注册后：postProcessBeanFactory");
//    }
//
//
//    /**
//     * Apply this {@code BeanPostProcessor} to the given new bean instance <i>before</i> any bean initialization callbacks (like InitializingBean's {@code afterPropertiesSet} or a custom init-method). The bean will already be populated with
//     * property values. The returned bean instance may be a wrapper around the original.
//     * <p>The default implementation returns the given {@code bean} as-is.
//     *
//     * @param bean     the new bean instance
//     * @param beanName the name of the bean
//     * @return the bean instance to use, either the original or a wrapped one; if {@code null}, no subsequent BeanPostProcessors will be invoked
//     * @throws BeansException in case of errors
//     * @see InitializingBean#afterPropertiesSet
//     */
//    @Override
//    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
//        System.out.println("初始化前");
//        return bean;
//    }
//
//    /**
//     * Apply this {@code BeanPostProcessor} to the given new bean instance <i>after</i> any bean initialization callbacks (like InitializingBean's {@code afterPropertiesSet} or a custom init-method). The bean will already be populated with
//     * property values. The returned bean instance may be a wrapper around the original.
//     * <p>In case of a FactoryBean, this callback will be invoked for both the FactoryBean
//     * instance and the objects created by the FactoryBean (as of Spring 2.0). The post-processor can decide whether to apply to either the FactoryBean or created objects or both through corresponding {@code bean instanceof FactoryBean}
//     * checks.
//     * <p>This callback will also be invoked after a short-circuiting triggered by a
//     * {@link InstantiationAwareBeanPostProcessor#postProcessBeforeInstantiation} method, in contrast to all other {@code BeanPostProcessor} callbacks.
//     * <p>The default implementation returns the given {@code bean} as-is.
//     *
//     * @param bean     the new bean instance
//     * @param beanName the name of the bean
//     * @return the bean instance to use, either the original or a wrapped one; if {@code null}, no subsequent BeanPostProcessors will be invoked
//     * @throws BeansException in case of errors
//     * @see InitializingBean#afterPropertiesSet
//     * @see FactoryBean
//     */
//    @Override
//    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//        System.out.println("初始化后");
//        return bean;
//    }
//}