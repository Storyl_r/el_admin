//package com.example.ms.service;
//
//import com.example.service.UserService;
//import javax.annotation.PreDestroy;
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.DisposableBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.config.BeanPostProcessor;
//import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
//import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
//import org.springframework.beans.factory.support.RootBeanDefinition;
//import org.springframework.stereotype.Component;
//
///**
// * 用户
// *
// * @author lr
// * @create 2023/1/1
// */
//@Component("orderService")
///**
// * InstantiationAwareBeanPostProcessor 实例化之前调用
// * MergedBeanDefinitionPostProcessor 对象实例化后，接下来就应该给对象的属性赋值了。在真正给属性赋值之前，Spring又提供了一个扩展点
// */
//public class OrderService implements BeanPostProcessor, InstantiationAwareBeanPostProcessor, MergedBeanDefinitionPostProcessor {
//
//    @Override
//    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
//        System.out.println("实例化之前====");
//        return null;
//    }
//
//    @Override
//    public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
//
//    }
//
//    /**
//     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
//     *
//     * @throws Exception in case of shutdown errors. Exceptions will get logged but not rethrown to allow other beans to release their resources as well.
//     */
////    @Override
////    public void destroy() throws Exception {
////        System.out.println("bean销毁");
////    }
//    public void test() {
//        System.out.println("orderService");
//    }
//
//    @Override
//    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
//
//        System.out.println("初始化后:" + beanName);
//        return null;
//    }
//
//    @Override
//    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//        System.out.println("初始化后");
//        return null;
//    }
//
//    @PreDestroy
//    public void close() {
//        System.out.println("容器关闭方法调用");
//    }
//}