package com.example.ms.jvm;

import com.example.api.entity.biz.User;
import org.openjdk.jol.info.ClassLayout;

public class ClassHead<obj> {
//    java.lang.Object object internals:
//    OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
//        0     4        (object header)                           01 00 00 00 (00000001 00000000 00000000 00000000) (1)
//            4     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
//            8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
//            12     4        (loss due to the next object alignment)
//    Instance size: 16 bytes
//    Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
//
//
//[I object internals:
//    OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
//        0     4        (object header)                           01 00 00 00 (00000001 00000000 00000000 00000000) (1)
//            4     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
//            8     4        (object header)                           6d 01 00 f8 (01101101 00000001 00000000 11111000) (-134217363)
//            12     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
//            16     0    int [I.<elements>                             N/A
//    Instance size: 16 bytes
//    Space losses: 0 bytes internal + 0 bytes external = 0 bytes total
//
//
//        20:33:52.429 [main] DEBUG org.apache.ibatis.logging.LogFactory - Logging initialized using 'class org.apache.ibatis.logging.slf4j.Slf4jImpl' adapter.
//    com.example.api.entity.biz.User object internals:
//    OFFSET  SIZE                            TYPE DESCRIPTION                               VALUE
//        0     4                                 (object header)                           01 00 00 00 (00000001 00000000 00000000 00000000) (1)
//            4     4                                 (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
//            8     4                                 (object header)                           1d cd 00 f8 (00011101 11001101 00000000 11111000) (-134165219)
//            12     4   org.apache.ibatis.logging.Log Model.log                                 (object)
//        16     4                java.lang.String BaseEntity.id                             null
//            20     4                java.lang.String BaseEntity.createId                       null
//            24     4                java.lang.String BaseEntity.createName                     null
//            28     4                java.lang.String BaseEntity.createNo                       null
//            32     4         java.time.LocalDateTime BaseEntity.createDate                     null
//            36     4                java.lang.String BaseEntity.updateId                       null
//            40     4                java.lang.String BaseEntity.updateName                     null
//            44     4         java.time.LocalDateTime BaseEntity.updateDate                     null
//            48     4                             int User.deleted                              0
//            52     4                             int User.version                              0
//            56     4                java.lang.String User.state                                null
//            60     4                java.lang.String User.userId                               null
//            64     4                java.lang.String User.name                                 null
//            68     4                java.lang.String User.headImgUrl                           null
//            72     4                java.lang.String User.mobile                               null
//            76     4                java.lang.String User.salt                                 null
//            80     4                java.lang.String User.password                             null
//            84     4                                 (loss due to the next object alignment)
//    Instance size: 88 bytes
//    Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
//
//
//    Process finished with exit code 0

    private static String lock = "";

    /**
     * 01：无锁  101偏向锁  00：轻量级锁   10：重量级锁 11：gc
     *
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(4000);

        synchronized (lock) {
            Object obj = new Object();
            System.out.println("new Object()");
            final ClassLayout classLayout = ClassLayout.parseInstance(new Object());
            System.out.println(classLayout.toPrintable());


            new Thread(() -> {
                System.out.println("Thread_one");
                final ClassLayout classLayout1 = ClassLayout.parseInstance(new Object());
                System.out.println(classLayout1.toPrintable());
            }, "Thread_one").start();

           // obj.wait(1000);
            System.out.println("new int[]{}");
            ClassLayout layout1 = ClassLayout.parseInstance(new int[]{});
            System.out.println(layout1.toPrintable());

            System.out.println(new User());
            ClassLayout layout2 = ClassLayout.parseInstance(new User());
            System.out.println(layout2.toPrintable());
        }
        Object obj = new Object();
        synchronized (obj) {
            // 思考：偏向锁执行过程中，调用hashcode会发生什么？
            //obj.hashCode();
            //obj.notify();
            try {
                obj.wait(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName() + "获取锁执行中。。。\n" + ClassLayout.parseInstance(obj).toPrintable());
        }
    }
}
