package com.example.ms.jvm;

import cn.hutool.json.JSONUtil;

import java.lang.ref.SoftReference;
import java.lang.reflect.Constructor;

/**
 * @author 类加载机制
 * @since 2023/8/9 11:59
 * 类的加载: 查找并加载类的二进制数据连接
 * 验证: 确保被加载的类的正确性准备: 为类的静态变量分配内存，并将其初始化为默认值解析: 把类中的符号引用转换为直接引用
 * 初始化：为类的静态变量赋予正确的初始值，JVM负责对类进行初始化，主要对类变量进行初始化。
 * 使用： 类访问方法区内的数据结构的接口， 对象是Heap区的数据卸载： 结束生命周期
 */
public class Bootstrap {

    public static void main(String[] args) throws Exception {
        //通过Class.forName的方式创建类 将类的.class文件加载到jvm中之外，还会对类进行解释，执行类中的static块
        Class<?> aClass = Class.forName("com.example.ms.jvm.Aa");
        Constructor constructor = aClass.getConstructor(String.class, Integer.class);
        Aa instance = (Aa) constructor.newInstance("你好", 1);
        instance.add();
        //只干一件事情，就是将.class文件加载到jvm中，不会执行static中的内容,只有在newInstance才会去执行static块
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        Class<?> loadClass = classLoader.loadClass("com.example.ms.jvm.Aa");
        Constructor constructor2 = loadClass.getConstructor(String.class, Integer.class);
        Aa instance2 = (Aa) constructor2.newInstance("你好", 1);
        instance2.add();
        //强引用
        Object o = new Object();

        // 被软引用关联的对象只有在内存不够的情况下才会被回收
        Object obj = new Object();
        SoftReference<Object> sf = new SoftReference<Object>(obj);
        // 使对象只被软引用关联
        obj = null;
    }
}
