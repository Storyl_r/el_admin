package com.example.ms.jvm;

/**
 * @author
 * @since 2023/8/9 14:33
 */
public class Aa {

    static String aa = "您好";

    public void add() {
        System.out.println("当前时间：{}" + System.currentTimeMillis());
    }

    private String age;
    private Integer sex;
    private Long height;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Aa(String age, Integer sex) {
        System.out.println("构造方法初始化============" + aa);
        this.age = age;
        this.sex = sex;
        add();
    }
}
