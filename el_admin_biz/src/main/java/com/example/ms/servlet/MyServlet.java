package com.example.ms.servlet;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author
 * @since 2023/8/30 9:11
 */
public class MyServlet extends HttpServlet {
    /**
     * 在web.xml里面配置
     * <servlet>
     *     <servlet-name>bb</servlet-name>
     *     <servlet-class>com.example.ms.servlet.MyServlet</servlet-class>
     * </servlet>
     * <servler-mapping>
     *     <servlet-name>bb</servlet>
     *     <url-pattern>/longin</url-pattern>
     * </servler-mapping>
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }
}
