package com.example.ms.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadOne {
    static Lock lock = new ReentrantLock();// 通过JDK5中的Lock锁来保证线程的访问的互斥
    private static int state = 1;//通过state的值来确定是否打印

    /**
     * java实现三线程按顺序轮流打印ABC100次的五种方法
     */
    static class ThreadA extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < 100; ) {
                try {
                    lock.lock();
                    while (state % 3 == 1) {// 多线程并发，不能用if，必须用循环测试等待条件，避免虚假唤醒
                        System.out.println("A");
                        state++;
                        i++;
                    }
                } finally {
                    lock.unlock();// unlock()操作必须放在finally块中
                }
            }
        }
    }

    static class ThreadB extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < 100; ) {
                try {
                    lock.lock();
                    while (state % 3 == 2) {
                        System.out.println("B");
                        state++;
                        i++;
                    }
                } finally {
                    lock.unlock();// unlock()操作必须放在finally块中
                }
            }
        }
    }

    static class ThreadC extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < 10; ) {
                try {
                    lock.lock();
                    while (state % 3 == 0) {
                        System.out.println("C");
                        state++;
                        i++;

                    }
                } finally {
                    lock.unlock();// unlock()操作必须放在finally块中
                }
            }
        }
    }


        static class ThreadD implements Callable<String> {
            @Override
            public String call() throws Exception {
                for (int i = 0; i < 10; ) {
                    try {
                        lock.lock();
                        while (state % 1 == 0) {
                            System.out.println("D");
                            state++;
                            i++;

                        }
                    } finally {
                        lock.unlock();// unlock()操作必须放在finally块中
                    }
                }
                return "";
            }
    }

    public static void main(String[] args) throws Exception {
        ThreadD d=new ThreadD();
        d.call();
      /*  // 获取 Java 线程管理 MXBean
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        // 不需要获取同步的 monitor 和 synchronizer 信息，仅获取线程和线程堆栈信息
        ThreadInfo[] threadInfos = threadMXBean.dumpAllThreads(false, false);
        // 遍历线程信息，仅打印线程 ID 和线程名称信息
        for (ThreadInfo threadInfo : threadInfos) {
            System.out.println("[" + threadInfo.getThreadId() + "] " + threadInfo.getThreadName());
        }
        Thread4 thread4 = new Thread4();
        Thread4 thread5 = new Thread4();
        try {
            Object call = thread4.call();
            Object call2 = thread5.call();
        } catch (
                Exception e) {
            e.printStackTrace();
        }*/
       /* HashMap hashMap = new HashMap(6);
        hashMap.put(null, "1");


        Hashtable hashtable = new Hashtable(6);
        hashtable.put(null, "1");*/
       /* new ThreadA().start();
        new ThreadB().start();
        new ThreadC().start();
        new Thread3().run();*/
    }

    /**
     * 线程二的创建方式
     */
    static class Thread3 implements Runnable {
        @Override
        public void run() {
            System.out.println("实现Runnable接口");
        }
    }

    /**
     * 线程三的创建方式
     */
    static class Thread4 implements Callable {
        @Override
        public Object call() throws Exception {
            int num = 0;
            for (int i = 0; i < 500000; i++) {
                num++;
            }
            System.out.println(num);
            return num;
        }
    }

}
