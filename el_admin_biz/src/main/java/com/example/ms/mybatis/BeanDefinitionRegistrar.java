package com.example.ms.mybatis;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

/**
 * @author
 * @since 2023/8/24 10:01
 * 注册bean实例
 */
public class BeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {


    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        //获取自定义的扫描路径
        Map<String, Object> attributes = importingClassMetadata.getAnnotationAttributes(MyComponentScan.class.getName());
        String path = (String) attributes.get("value");
        MyBeanDefinitionScanner scanner = new MyBeanDefinitionScanner(registry);
        //直接扫描全部类
        scanner.addExcludeFilter((metadataReader, metadataReaderFactory) -> true);
         scanner.scan(path);
    }
}
