package com.example.ms.mybatis;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author
 * @since 2023/8/24 9:51
 */
@ComponentScan("com.example.ms.mybatis")
@MyComponentScan("com.example.ms.mybatis.mapper")
public class MyConfig {
}
