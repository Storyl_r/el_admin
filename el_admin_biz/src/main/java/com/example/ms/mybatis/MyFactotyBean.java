package com.example.ms.mybatis;

import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author 模拟spring加载mybatis代理对象
 * @since 2023/8/24 9:27
 */
public class MyFactotyBean implements FactoryBean {

    private Class mapperInterface;


    public MyFactotyBean(Class mapperInterface) {
        this.mapperInterface = mapperInterface;
    }


    @Override
    public Object getObject() throws Exception {
        Object proxyInstance = Proxy.newProxyInstance(MyFactotyBean.class.getClassLoader(), new Class[]{mapperInterface}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                return null;
            }
        });
        return proxyInstance;
    }

    @Override
    public Class<?> getObjectType() {
        return mapperInterface;
    }
}
