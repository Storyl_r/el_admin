package com.example.ms.io;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * Nio的工作流程
 */
public class Nio {
    public static void main(String[] args) throws Exception {
        //创建Nio 通道
        final ServerSocketChannel openService = ServerSocketChannel.open();
        //开放端口
        openService.socket().bind(new InetSocketAddress(9000));
        //设置为非阻塞
        openService.configureBlocking(false);
        //创建选择区
        final Selector selector = Selector.open();
        //进行绑定并且对客户端请求进行监听
        openService.register(selector, SelectionKey.OP_ACCEPT);

        while (true) {
            int select = selector.select();
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                if (key.isAcceptable()) {
                    //得到客户端请求
                    ServerSocketChannel server = (ServerSocketChannel) key.channel();
                    SocketChannel accept = server.accept();
                    accept.configureBlocking(false);
                    //注册读事件
                    accept.register(selector, SelectionKey.OP_READ);
                    System.out.println("客户端连接成功");
                } else if (key.isReadable()) {
                    SocketChannel socketChannel = (SocketChannel) key.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(128);
                    int read = socketChannel.read(buffer);
                    if (read > 0) {
                        System.out.println("接收到消息：" + new String(buffer.array()));
                    } else if (read == -1) {
                        System.out.println("断开连接");
                        socketChannel.close();
                    }
                }
                iterator.remove();
            }

        }
    }
}
