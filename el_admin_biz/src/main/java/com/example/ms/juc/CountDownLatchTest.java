package com.example.ms.juc;

import lombok.SneakyThrows;

import java.util.concurrent.*;

public class CountDownLatchTest {

    static CountDownLatch latch = new CountDownLatch(1);
    static final Semaphore semaphore = new Semaphore(1);

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        System.out.println("主线程等待 latch 解阻塞...");
        // 创建一个线程来执行 latch.countDown()
        for (int i = 0; i < 3; i++) {
            int finalI = i;
            new Thread(() -> {
                try {
                    latch.await();
                    System.out.println("人员" + finalI + "已到位"+System.currentTimeMillis());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        Thread.sleep(100);
        latch.countDown();
        System.out.println(Thread.currentThread().getName() + " 调用 countDown()");
        System.out.println("latch 已解阻塞，主线程继续执行");
        // 主线程继续执行
        Callable<String> callable = new A();
        FutureTask<String> futureTask = new FutureTask<>(callable);
        Thread thread = new Thread(futureTask);
        thread.start();
        System.out.println(futureTask.get() + "执行完成");


        System.out.println("------------------------------------------");
        System.out.println("---------------- semaphore ---------------");
        System.out.println("------------------------------------------");
        for (int i = 0; i < 5; i++) {
            new Thread(new Worker("Worker-" + i)).start();
        }
        System.out.println("------------------------------------------");
        System.out.println("---------------- CyclicBarrier ---------------");
        System.out.println("------------------------------------------");
        //创建一个为5的临界点
        CyclicBarrier barrier = new CyclicBarrier(5, new Runnable() {
            @Override
            public void run() {
                System.out.println("所有运动员都已准备好，比赛开始！");
            }
        });

        for (int i = 0; i < 5; i++) {
            new Thread(new yundongyuan("运动员" + i, barrier)).start();
        }
    }

    static class yundongyuan implements Runnable {
        private final String name;
        private final CyclicBarrier cyclicBarrier;

        public yundongyuan(String name, CyclicBarrier cyclicBarrier) {
            this.name = name;
            this.cyclicBarrier = cyclicBarrier;
        }

        @SneakyThrows
        @Override
        public void run() {
            System.out.println(name + "准备好了");
            // 在所有线程都到达屏障点后，等待屏障任务执行完再继续
            synchronized (System.out) {
                System.out.println(name + " 开始跑！");
            }
        }
    }

    static class A implements Callable<String> {
        @Override
        public String call() throws Exception {
            return "主线程执行完成";
        }
    }

    /**
     * 模拟线程 一次只允许最多两个线程获取锁
     */
    static class Worker implements Runnable {
        private final String name;

        Worker(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            try {
                // 尝试获取许可证
                semaphore.acquire();
                // 模拟任务执行
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                // 释放许可证
                semaphore.release();
                System.out.println(name + " 任务执行完成，释放许可证");
            }
        }
    }
}
