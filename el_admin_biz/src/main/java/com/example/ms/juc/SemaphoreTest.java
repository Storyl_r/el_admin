package com.example.ms.juc;

import java.util.concurrent.Semaphore;

/**
 * 交替打印ABC
 */
public class SemaphoreTest {
    private static final Semaphore sa1 = new Semaphore(1);
    private static final Semaphore sa2 = new Semaphore(1);
    private static final Semaphore sa3 = new Semaphore(1);

    public static void main(String[] args) throws Exception {
        sa1.acquire();
        sa2.acquire();

        new Thread(() -> {
            while (true) {
                try {
                    sa3.acquire();
                    System.out.println("A");
                    Thread.sleep(500);
                    sa1.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }).start();

        new Thread(() -> {
            while (true) {
                try {
                    sa1.acquire();
                    System.out.println("B");
                    Thread.sleep(500);
                    sa2.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            while (true) {
                try {
                    sa2.acquire();
                    System.out.println("C");
                    Thread.sleep(500);
                    sa3.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
