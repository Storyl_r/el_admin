package com.example.ms.juc;

public class VolatileTest {

    static volatile int start = 1;

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            while (true) {
                if (start == 1) {
                    try {
                        Thread.sleep(500);
                        for (int i = 0; i < 10; i++) {
                            System.out.println("A" + i);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    start = 2;
                    return;
                }

            }

        });
        Thread thread2 = new Thread(() -> {
            while (true) {
                if (start == 2) {
                    try {
                        Thread.sleep(500);
                        for (int i = 0; i < 10; i++) {
                            System.out.println("B" + i);
                        }
                        start = 3;
                        return;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

        Thread thread3 = new Thread(() -> {
            while (true) {
                if (start == 3) {
                    try {
                        Thread.sleep(500);
                        for (int i = 0; i < 10; i++) {
                            System.out.println("C" + i);
                        }
                        start = 1;
                        return;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
        thread1.start();
        thread2.start();
        thread3.start();


    }
}
