package com.example.ms;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;

/**
 * A
 *
 * @author lr
 * @create 2023/1/29
 */
public abstract class A {

    private LocalDateTime updateDate;

    private String aVoid;

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public String getaVoid() {
        return aVoid;
    }

    public void setaVoid(String aVoid) {
        this.aVoid = aVoid;
    }
}