package com.example.ms.pdf;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;

public class ImageWatermarkUtils {


    public static void main(String[] args) throws Exception {


        //String url1 = "D:\\pdf\\a.jpg";
        File file=new File(new URI("http://image.hnol.net/c/2020-08/22/16/c2ab1cdafc3e54fe6e5597f0cb3fec0a-6072437.jpg"));
        ByteArrayOutputStream byteArrayOutputStream = ImageWatermarkUtils.markWithContent(file, FONT, Color.darkGray, "AAAAAAAAAA");
        System.out.println(byteArrayOutputStream);
        try (FileOutputStream fos = new FileOutputStream("D:\\pdf\\bbb.jpg")) {
            byteArrayOutputStream.writeTo(fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 水印字体
    private static final Font FONT = new Font("微软雅黑", Font.PLAIN, 19);

    // 透明度
    private static final AlphaComposite COMPOSITE = AlphaComposite
            .getInstance(AlphaComposite.SRC_OVER, 0.6f);

    // 水印之间的间隔
    private static final int X_MOVE = 300;

    // 水印之间的间隔
    private static final int Y_MOVE = 300;

    /**
     * 打水印(文字)
     *
     * @param font             字体
     * @param markContentColor 水印颜色
     * @param waterMarkContent 水印内容
     */
    public static ByteArrayOutputStream markWithContent(File srcFile, Font font, Color markContentColor,

                                                        String waterMarkContent) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            // 读取原图片信息
            BufferedImage srcImg = ImageIO.read(srcFile);

            // 图片宽、高
            int imgWidth = srcImg.getWidth();
            int imgHeight = srcImg.getHeight();

            // 图片缓存
            BufferedImage bufImg = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);

            // 创建绘图工具
            Graphics2D graphics = bufImg.createGraphics();

            // 画入原始图像
            graphics.drawImage(srcImg, 0, 0, imgWidth, imgHeight, null);

            // 设置水印颜色
            graphics.setColor(markContentColor);

            // 设置水印透明度
            graphics.setComposite(COMPOSITE);

            // 设置倾斜角度
            graphics.rotate(Math.toRadians(-35), (double) bufImg.getWidth() / 2,
                    (double) bufImg.getHeight() / 2);

            // 设置水印字体
            graphics.setFont(font);

            // 消除java.awt.Font字体的锯齿
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            int xCoordinate = -imgWidth / 2, yCoordinate;
            // 字体长度
            int markWidth = FONT.getSize() * getTextLength(waterMarkContent);
            // 字体高度
            int markHeight = FONT.getSize();

            // 循环添加水印
            while (xCoordinate < imgWidth * 1.5) {
                yCoordinate = -imgHeight / 2;
                while (yCoordinate < imgHeight * 1.5) {
                    graphics.drawString(waterMarkContent, xCoordinate, yCoordinate);
                    yCoordinate += markHeight + Y_MOVE;
                }
                xCoordinate += markWidth + X_MOVE;
            }

            // 释放画图工具
            graphics.dispose();
            //返回流
            ImageIO.write(bufImg, "jpg", baos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                try {
                    baos.flush();
                    baos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return baos;
    }


    //计算水印文本长度
    //1、中文长度即文本长度 2、英文长度为文本长度二分之一
    public static int getTextLength(String text) {
        //水印文字长度
        int length = text.length();

        for (int i = 0; i < text.length(); i++) {
            String s = String.valueOf(text.charAt(i));
            if (s.getBytes().length > 1) {
                length++;
            }
        }
        length = length % 2 == 0 ? length / 2 : length / 2 + 1;
        return length;
    }


}
