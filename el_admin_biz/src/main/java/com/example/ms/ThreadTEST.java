package com.example.ms;

import java.util.concurrent.*;

/**
 * Spring启动流程测试
 *
 * @author lr
 * @create 2023/1/1
 */
public class ThreadTEST extends Thread {
 /*   AnnotationConfigApplicationContext applicationContext=new AnnotationConfigApplicationContext(com.example.mianshi.AppConfig.class);
    final User user = (User)applicationContext.getBean("user");*/

    public static class TestRuntimeExceptionThread extends Thread {

        // 指定线程名称 便于问题定位
        public TestRuntimeExceptionThread() {
            super.setName("thread-TestRuntimeExceptionThread");
        }

        @Override
        public void run() {
            throw new RuntimeException("run time exception");
        }

        public static void main(String[] args) {

            createThread();

            // newFixedThreadPool();
        }

        /**
         * 创建线程池
         */
        private static void createThread() {
            TestRuntimeExceptionThread test = new TestRuntimeExceptionThread();
            test.setUncaughtExceptionHandler((Thread t, Throwable throwable) -> {
                System.out.println(String.valueOf(null));
                System.out.println(t.getName() + ":：：：：：： " + throwable.getMessage());
            });
            test.start();
//        ThreadFactory factory = r -> new Thread(r, "test");
//        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(1, 200, 1,
//                                                               TimeUnit.SECONDS, new ArrayBlockingQueue<>(100), factory, new ThreadPoolExecutor.CallerRunsPolicy());
//        threadPool.prestartAllCoreThreads();
//        final int[] g = {0};
//        ReentrantLock lock=new ReentrantLock();
//        for (int i = 0; i < 10000; i++) {
//            Future submit = threadPool.submit(() -> {
//                System.out.println("线程名称：" + Thread.currentThread().getName());
//                Object o = null;
//                g[0]++;
//               // System.out.println(o.toString());
//                System.out.println(JSONUtil.toJsonStr(threadPool));
//                System.out.println(JSONUtil.toJsonStr(threadPool.getThreadFactory().toString()));
//
//            });
//            try {
//                lock.lock();
//                submit.get();
//            } catch (Exception e) {
//                System.out.println("程序执行异常：" + e);
//
//            }finally {
//                lock.unlock();
//            }
//
//        }
//
//        threadPool.shutdown();
//        System.out.println(g[0]);
        }

        private static void newFixedThreadPool() {
            ExecutorService executor = Executors.newScheduledThreadPool(10);
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                executor.execute(() -> {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        //do nothing
                    }
                });
            }
        }


    }
}
