package com.example.easyexcel;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author lr
 * @since 2021-04-17
 */
@TableName("user_info")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserInfoModel {

    @ExcelIgnore // 生成报表时忽略，不生成次字段
    private String id;
    @ExcelProperty(value = "姓名", index = 0) // 定义表头名称和位置,0代表第一列
    private String userName;
    @ExcelProperty(value = "密码", index = 1) // 定义表头名称和位置,0代表第一列
    private String password;

    public UserInfoModel(List<GrantedAuthority> userAuthority) {
    }
}
