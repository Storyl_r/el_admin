//package com.example.seata;
//
//import javax.servlet.Servlet;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.servlet.DispatcherServlet;
//
///**
// * @author lr
// */
//@Slf4j
//@Configuration
//@ConditionalOnClass({Servlet.class, DispatcherServlet.class})
//@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
//@RestControllerAdvice
//public class LxtxRestExceptionTranslator {
//    /**
//     * 功能描述：全局异常处理
//     *
//     * @param e
//     * @return 返回处理结果
//     * @throws Exception
//     */
//   /* @ExceptionHandler(value = Exception.class)
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    public R errorHandler(Exception e) throws Exception {
//        // 此处为属性级的错误日志的处理
//        if (e instanceof ConstraintViolationException) {
//            log.info("绑定错误日志为：{}", e.getMessage());
//            return R.failed("请求数据格式错误");
//            // 此处为方法级别的错误日志处理
//        } else if (e instanceof MethodArgumentNotValidException) {
//            log.info("方法级的绑定错误日志为：{}", e.getMessage());
//            return R.failed("请求数据格式错误");
//            // 此处为全局错误日志的处理
//        } else {
//            log.info("错误日志为：{}", e.getMessage());
//            return R.failed("未知服务器异常");
//        }
//    }*/
//}
