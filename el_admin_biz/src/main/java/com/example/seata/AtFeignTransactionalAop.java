//package com.example.seata;
//
//
//import com.aliyun.openservices.shade.org.apache.commons.lang3.StringUtils;
//import com.baomidou.mybatisplus.extension.enums.ApiErrorCode;
//import io.seata.core.context.RootContext;
//import io.seata.core.exception.TransactionException;
//import io.seata.tm.api.GlobalTransaction;
//import io.seata.tm.api.GlobalTransactionContext;
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.annotation.AfterReturning;
//import org.aspectj.lang.annotation.AfterThrowing;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.reflect.MethodSignature;
//import org.springframework.stereotype.Component;
//
//import java.lang.reflect.Method;
//import java.util.Map;
//
///**
// * @Aspect:作用是把当前类标识为一个切面供容器读取
// * @Component 标注该类是被spring容器进行管理的
// */
//@Slf4j
//@Aspect
//@Component
//public class AtFeignTransactionalAop {
//
//    @Before("execution(* com.example.controller.*.*(..))")
//    public void before(JoinPoint joinPoint) throws TransactionException, io.seata.core.exception.TransactionException {
//        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
//        Method method = signature.getMethod();
//        log.info("拦截到需要分布式事务的方法," + method.getName());
//        GlobalTransaction tx = GlobalTransactionContext.getCurrentOrCreate();
//        // 超时时间 , 所在服务
//        tx.begin(3000000, "el_admin_biz");
//        log.info("创建分布式事务id:{}", tx.getXid());
//    }
//
//    @AfterThrowing(throwing = "e", pointcut = "execution(* com.example.controller.*.*(..))")
//    public void doRecoveryActions(Throwable e) throws TransactionException, io.seata.core.exception.TransactionException {
//        log.info("方法执行异常:{}", e.getMessage());
//        if (!StringUtils.isBlank(RootContext.getXID())) {
//            log.info("分布式事务Id:{}, 手动回滚!", RootContext.getXID());
//            GlobalTransactionContext.reload(RootContext.getXID()).rollback();
//        }
//    }
//
//    @AfterReturning(value = "execution(* com.example.controller.*.*(..))", returning = "result")
//    public void afterReturning(JoinPoint point, Object result) throws TransactionException, io.seata.core.exception.TransactionException {
//        log.info("方法执行结束:{}", result);
//        Map map = new org.apache.commons.beanutils.BeanMap(result);
//        long status = ApiErrorCode.SUCCESS.getCode();
//        if (map.size() > 1) {
//            status = Integer.parseInt(map.get("code").toString());
//        }
//        if (status != Long.valueOf(map.get("code").toString())) {
//            if (!StringUtils.isBlank(RootContext.getXID())) {
//                log.info("分布式事务Id:{}, 手动回滚!", RootContext.getXID());
//                GlobalTransactionContext.reload(RootContext.getXID()).rollback();
//            }
//        }
//    }
//}
