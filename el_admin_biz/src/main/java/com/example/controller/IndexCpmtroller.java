package com.example.controller;

import common.annotations.Inner;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author
 * @since 2023/4/18 10:39
 */
@Controller
public class IndexCpmtroller {
    @GetMapping(value = "/index")
    @Inner
    public String index() {
        return "/index";
    }
}
