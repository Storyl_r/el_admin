package com.example.controller;

import com.example.api.common.R;
import com.example.api.entity.biz.Account;
import com.example.service.AccountService;
import com.example.service.ThreadPoolExecutorService;
import common.annotations.Inner;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author lr
 * @description 账号
 * @date 2022-03-18
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(value = "/account")
public class AccountController {

    private final AccountService accountService;

    private final ThreadPoolExecutorService executorService;


    @RequestMapping("/createScheduledExecutorService")
    public void initsss() {
        ScheduledExecutorService scheduledExecutorService = executorService.executeScheduledTaskThread(1);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                log.info("执行定时任务");
                scheduledExecutorService.shutdown();
            }
        };
        //3秒后执行 间隔3000执行一次
        scheduledExecutorService.scheduleAtFixedRate(timerTask, 3, 3000, TimeUnit.MILLISECONDS);

    }

    /**
     * 新增
     *
     * @author lr
     * @date 2022/03/18
     **/
    @RequestMapping("/insert")
    public Object insert(Account account) {
        return accountService.insert(account);
    }

    /**
     * 刪除
     *
     * @author lr
     * @date 2022/03/18
     **/
    @RequestMapping("/delete")
    public R<Object> delete(int id) {
        return R.ok(accountService.delete(id));
    }


    /**
     * 查询 根据主键 id 查询
     *
     * @author lr
     * @date 2022/03/18
     **/
    @RequestMapping("/load")
    public Object load(int id) {
        return accountService.load(id);
    }

    /**
     * 查询 分页查询
     *
     * @author lr
     * @date 2022/03/18
     **/
    @RequestMapping("/pageList")
    public Map<String, Object> pageList(@RequestParam(required = false, defaultValue = "0") int offset,
                                        @RequestParam(required = false, defaultValue = "10") int pagesize) {
        return accountService.pageList(offset, pagesize);
    }

}