package com.example.controller;

import com.example.api.entity.biz.User;
import com.example.api.vo.biz.UserVo;
import com.example.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import utils.BeanUtils;
import utils.excel.ExcelHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author lr
 * @description 账号
 * @date 2022-03-18
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/excl")
@Slf4j
public class ExcelController {

    private final UserService userService;

    /**
     * 新增
     *
     * @author lr
     * @date 2022/03/18
     **/
    @RequestMapping("/impl")
    public Object insert() throws IOException {
        ClassPathResource cpr = new ClassPathResource("excl/sys_user_t.xls");
        List<Object> list = ExcelHelper.readMoreThan1000Row(cpr.getURL().getPath());
        for (Object x : Collections.unmodifiableList(list)) {
            System.out.println("读取到的文件：" + x.toString());
        }
        return null;
    }

    /**
     * 新增
     *
     * @author lr
     * @date 2022/03/18
            **/
    @RequestMapping("/export")
    public void export(@RequestParam("filedNames") String filedNames, String filedCodes) throws IOException {
        String[] head = filedNames.split(",");
        List<String> headList = new ArrayList<>(Arrays.asList(head));

        String[] file = filedCodes.split(",");
        List<String> fileList = new ArrayList<>(Arrays.asList(file));
        List<User> userList = userService.getBaseMapper().selectList(null);
        List<UserVo> userVos = BeanUtils.toBeanList(userList, UserVo.class);
        ExcelHelper.writeExcelIgnoreFields("用户.xls", "用户", userVos, headList, fileList);
    }

}