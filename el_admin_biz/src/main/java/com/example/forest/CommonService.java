package com.example.forest;

import com.dtflys.forest.annotation.Request;
import com.example.api.entity.order.OrderInfo;

import java.util.List;

/**
 * 远程调用服务
 *
 * @author lr
 * @create 2022/3/18
 */
public interface CommonService {

    /**
     * 取订单信息
     *
     * @return
     */
    @Request(ServiceUrl.searchOrder)
    List<OrderInfo> getList();
}