package com.example.test.classforname;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Administrator
 */
@Data
public class ClassDto {
    @NotBlank(message = "班级名称不能为空")
    private String className;
    @NotBlank(message = "班级号不能为空")
    private String classNo;
    @NotBlank(message = "年级不能为空")
    private String nj;

    public ClassDto() {
        this.className = "一年级";
        this.classNo = "1102";
        this.nj = "7";
    }
}
