package com.example.test.classforname;

import javax.validation.constraints.NotBlank;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class GetClass {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class aClass = Class.forName("com.example.test.classforname.ClassDto");
        Field className = aClass.getDeclaredField("className");
        NotBlank annotation = className.getAnnotation(NotBlank.class);
        System.out.println(annotation.message());
        Constructor constructor = aClass.getConstructor();
        Object o = constructor.newInstance();
        if (o instanceof ClassDto) {
            System.out.println("yes================");
        } else {
            System.out.println("no===============");
        }
    }
}


