package com.example;

import com.example.util.JwtUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Administrator
 */
@SpringBootApplication

//开启figen远程调用@EnableDiscoveryClient
@EnableFeignClients
//@EnableBinding(ChatProcessor.class)
/**
 * 服务降级或熔断
 */
//@EnableHystrix
//开启日志监控
public class ElAdminBizApplication  {

    public static void main(String[] args) {
//        System.out.println("SpringBootVersion：" + SpringBootVersion.getVersion() + ";SpringVersion:" + SpringVersion.getVersion());
//        MyEvent event = new MyEvent(new Object());
//        event.setMessage("event====================事件发送");

        SpringApplication.run(ElAdminBizApplication.class, args);
        // 发布一个事件 ApplicationContext 是一个容器bean 可以再其它地方拿到 用它发布事件即可
        // run.publishEvent(event);

    }

    /**
     * 用于加密
     *
     * @return
     */
    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @ConfigurationProperties("jwt.config")
    public JwtUtil jwtUtil() {
        return new JwtUtil();
    }

}