package com.example.config.redisandlua;//package com.example.config.redisandlua;
//
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
//import org.springframework.beans.factory.support.BeanDefinitionRegistry;
//import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
//import org.springframework.beans.factory.support.RootBeanDefinition;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @Author Story
// * @Date 2022/3/20 6:13
// * @Version 1.0
// */
//@Configuration
//public class DefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
//    @Override
//    public void postProcessBeanFactory(ConfigurableListableBeanFactory arg0) throws BeansException {
//
//    }
//
//    /**
//     * 先执行postProcessBeanDefinitionRegistry方法
//     * 在执行postProcessBeanFactory方法
//     */
//    @Override
//    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry)
//            throws BeansException {
//        // 第一种 ： 手动注入
//        // 注册bean
//      //  registerBean(registry, "accessLimiter", AccessLimiter.class);
//    }
//
//    /**
//     * 注册bean
//     **/
//    private void registerBean(BeanDefinitionRegistry registry, String name, Class<?> beanClass) {
//        RootBeanDefinition bean = new RootBeanDefinition(beanClass);
//        registry.registerBeanDefinition(name, bean);
//    }
//}
