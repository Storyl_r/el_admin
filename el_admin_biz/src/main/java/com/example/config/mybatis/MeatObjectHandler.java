package com.example.config.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import java.time.LocalDateTime;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

/**
 * mybatisPlus属性自动填充，对应的实体类字段上需要加@TableField(fill = FieldFill.INSERT_UPDATE)
 */
@Component
public class MeatObjectHandler implements MetaObjectHandler {

    /**
     * 插入时自动填充
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        LocalDateTime now = LocalDateTime.now();
        setFieldValByName("createDate", now, metaObject);
        setFieldValByName("updateDate", now, metaObject);
        // todo 后续需要获取当前登录人信息进行属性填充
    }

    /**
     * 更新时自动填充
     *
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        setFieldValByName("updateDate", LocalDateTime.now(), metaObject);
        // todo 后续需要获取当前登录人信息进行属性填充
    }
}
