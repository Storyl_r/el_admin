package com.example.config.event;

import org.springframework.context.ApplicationEvent;

/**
 * 自定义监听器
 *
 * @author lr
 * @create 2022/3/30
 */
public class MyEvent extends ApplicationEvent {

    private String message;

    public MyEvent(Object source) {
        super(source);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}