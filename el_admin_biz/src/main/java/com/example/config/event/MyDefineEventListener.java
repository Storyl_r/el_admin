package com.example.config.event;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * 自定义监听器
 *
 * @author lr
 * @create 2022/3/30
 */
@Component
public class MyDefineEventListener implements ApplicationListener<MyEvent> {

    @Override
    @EventListener
    public void onApplicationEvent(MyEvent event) {
        System.out.println(event.getMessage());
    }
}