package com.example.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author
 * @since 2023/8/21 15:50
 */

@Configuration
@EnableConfigurationProperties(DemoProperties.class)
public class DemoAutoConfiguration {

    @Bean
    public void testBeam() {
        System.out.println("==========================");
        System.out.println("============自动加载bean==============");
        System.out.println("==========================");
    }
}
