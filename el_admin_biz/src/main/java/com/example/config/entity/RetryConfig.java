package com.example.config.entity;

/**
 * @author lr
 * @create 2023/2/5
 */
public class RetryConfig {

    private String lastDdl;
    private String retryRequest;
    private String startTime;

    public String getLastDdl() {
        return lastDdl;
    }

    public void setLastDdl(String lastDdl) {
        this.lastDdl = lastDdl;
    }

    public String getRetryRequest() {
        return retryRequest;
    }

    public void setRetryRequest(String retryRequest) {
        this.retryRequest = retryRequest;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}