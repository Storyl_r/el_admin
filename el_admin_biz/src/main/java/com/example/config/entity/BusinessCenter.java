package com.example.config.entity;

import lombok.Data;

/**
 * 业务中心
 *
 * @author lr
 * @create 2022/3/11
 */
@Data
public class BusinessCenter {

    /**
     * 用户编号
     */
    private String userNo;
    /**
     * 用户姓名
     */
    private String userName;
    /**
     * 所属中心
     */
    private String core;
}