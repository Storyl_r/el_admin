//package com.example.config.entity;
//
//import common.annotations.Inner;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * 根据配置信息初始化bean
// *
// * @author lr
// * @create 2022/4/15
// */
//@Configuration
//@ConditionalOnProperty(prefix = "el_admin_biz", name = "judge", matchIfMissing = true)
//public class Judge {
//
//    @Bean
//    @Inner
//    public void judges() {
//        System.out.println("初始化bean");
//        System.out.println("初始化bean");
//        System.out.println("初始化bean");
//        System.out.println("初始化bean");
//    }
//}