package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.api.entity.biz.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Story
 * @Date 2022/3/19 23:23
 * @Version 1.0
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    Integer updateUser(@Param("user") User user);
}
