package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.api.entity.biz.Account;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @description 账号
 * @author lr
 * @date 2022-03-18
 */
@Mapper
public interface AccountMapper extends BaseMapper<Account> {


    /**
     * 刪除
     * @author lr
     * @date 2022/03/18
     **/
    int delete(int id);


    /**
     * 查询 根据主键 id 查询
     * @author lr
     * @date 2022/03/18
     **/
    Account load(int id);

    /**
     * 查询 分页查询
     * @author lr
     * @date 2022/03/18
     **/
    List<Account> pageList(int offset, int pagesize);

    /**
     * 查询 分页查询 count
     * @author lr
     * @date 2022/03/18
     **/
    int pageListCount(int offset,int pagesize);

}