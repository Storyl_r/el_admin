package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.api.entity.biz.SysUserRegister;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户注册信息 Mapper 接口
 * </p>
 *
 * @author lr
 * @since 2022-03-26
 */
@Mapper
public interface SysUserRegisterMapper extends BaseMapper<SysUserRegister> {

}
