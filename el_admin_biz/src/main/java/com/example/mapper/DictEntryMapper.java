package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.api.entity.biz.SysDict;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lr
 * @description 账号
 * @date 2022-03-18
 */
@Mapper
public interface DictEntryMapper extends BaseMapper<SysDict> {


}