package com.example.annotations;

import java.lang.annotation.*;

/**
 * @Author: lr
 * @Description:
 */

@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JsonToObject {


}