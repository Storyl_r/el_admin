package com.example.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 调用重试
 *
 * @author lr
 * @create 2023/2/5
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RpcRetry {

    int retryTimes() default 3;

    int ddl() default 3000;

    // 返回给上一层的
    boolean noMoreRetry() default true;
}