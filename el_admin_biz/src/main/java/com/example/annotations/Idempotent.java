package com.example.annotations;

import java.lang.annotation.*;

/**
 * 接口幂等
 *
 * @author
 * @since 2023/4/7 9:33
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Idempotent {
}
