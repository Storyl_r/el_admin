package com.example.cart;

import org.springframework.stereotype.Service;

/**
 * @author
 * @since 2023/3/28 10:46
 */
@Service
public class TbItemService {
    /**
     * @param itemId
     */
    public TbItem getItemById(Long itemId) {
        TbItem build = new TbItem();
        build.setBarcode("1");
        build.setTitle("小东西");
        build.setId(itemId);
        return build;
    }
}
