package com.example.cart;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.example.api.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @since 2023/3/28 10:45
 */
@Controller
public class ShopCartController {

    private final static String CART_REDIS_KEY_PRE = "cart_redis_key";

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private TbItemService tbItemService;

    @Autowired
    private ShopCartService shopCartService;

    @Value("${EXPIRE_KEY}")
    private Integer EXPIRE_KEY;

    @Value("${CART_COOKIE}")
    private String CART_COOKIE;

    /**
     * 需求：将商品加入购物车中未登录状态下，将购物超过添加到cookie中
     * <p>
     * 分析：1、从cookie中获取购物车信息
     * 2、判断购物车中的商品，如果添加的商品存在，数量相加，不存在，根据商品id查询商品信息，添加到cookie中
     * 3、将购物车列表信息写入cookie中
     *
     * @param itemId
     * @param num
     * @return
     */
    @RequestMapping("/cart/add/{itemId}")
    public String addCart(@PathVariable Long itemId, @RequestParam(defaultValue = "1") Integer num,
                          HttpServletRequest request, HttpServletResponse response) {
        // 1.获得购物车列表
        List<TbItem> itemList = getCartItemList(request);
        // 用来判断商品是否存在的标志
        boolean flag = false;
        // 2、循环遍列表中的商品信息
        for (TbItem tbItem : itemList) {
            // 3、判断添加的商品是否存在
            if (tbItem.getId() == itemId.longValue()) {
                // 4、添加的商品在cookie中存在，将数量相加
                tbItem.setNum(tbItem.getNum() + num);
                // 重置标签
                flag = true;
                // 跳出循环
                break;
            }
        }
        if (!flag) {
            // cookie中没有添加的商品信息 通过商品id查询商品信息
            TbItem item = tbItemService.getItemById(itemId);
            item.setNum(num);
            if (StrUtil.isNotBlank(item.getImage())) {
                // 取一张图片用于展示使用
                item.setImage(item.getImage().split(",")[0]);
            }
            // 将商品添加购物车
            itemList.add(item);
        }
        //将购物车写入cookie中

        CookieUtil.set(response, CART_COOKIE, JSONUtil.toJsonStr(itemList), true);
        return "cartSuccess";

    }

    /**
     * 从cookie取商品信息
     *
     * @param request
     * @return
     */
    private List<TbItem> getCartItemList(HttpServletRequest request) {
        String json = CookieUtil.getValue(request, CART_COOKIE);
        if (StrUtil.isNotBlank(json)) {
            return JSONUtil.toList(json, TbItem.class);
        }
        return new ArrayList<>();
    }

    /**
     * 获取购物车列表(从cookie里面)
     *
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping("/cart/cart")
    public R getCartList(HttpServletRequest request, HttpServletResponse response, Model model) {
        List<TbItem> itemList = getCartItemList(request);
        return R.ok(itemList);
    }

    @RequestMapping("/cart/delete/{itemId}")
    public R deleteCartItem(@PathVariable Long itemId, HttpServletRequest request, HttpServletResponse response) {
        List<TbItem> list = getCartItemList(request);
        for (TbItem tbItem : list) {
            if (tbItem.getId() == itemId.longValue()) {
                list.remove(tbItem);
                break;
            }
        }
        // 删除成功后，将购物车列表写入cookie中
        CookieUtil.set(response, CART_COOKIE, JSONUtil.toJsonStr(list), true);

        // 删除成功后，重定向到购物车列表页面
        return R.ok("删除成功");

    }


    /**
     * 登录后的加入购物车逻辑
     *
     * @param userId
     * @param itemId
     * @param num
     * @return
     */
    public R addlogCart(Long userId, Long itemId, Integer num) {
        try {
            // 从redis中取出购物车，判断是否已经有购物项
            Boolean hexists = redisTemplate.opsForHash().hasKey(CART_REDIS_KEY_PRE + ":" + userId, itemId + "");
            if (hexists) {
                // 表示购物车中已经有该商品，只需要将该商品的数量相加即可
                Object hget = redisTemplate.opsForHash().get(CART_REDIS_KEY_PRE + ":" + userId, itemId + "");
                // 将数量相加
                TbItem item = JSONUtil.toBean((JSONObject) hget, TbItem.class);
                item.setNum(item.getNum() + num);
                // 将商品重新放入购物车中
                redisTemplate.opsForHash().put(CART_REDIS_KEY_PRE + ":" + userId, itemId, JSONUtil.toJsonStr(item));
                return R.ok("购物车添加成功");
            }
            // 表示购物车中没有要添加的商品信息 根据商品的id查询商品的信息(查数据库)
            TbItem item = this.selectByPrimaryKey(itemId);
            item.setNum(num);
            if (StrUtil.isNotBlank(item.getImage())) {
                item.setImage(item.getImage().split(",")[0]);
            }
            // 将商品信息存入购物车中
            redisTemplate.opsForHash().put(CART_REDIS_KEY_PRE + ":" + userId, itemId, JSONUtil.toJsonStr(item));
            return R.ok("购物车添加成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.failed("商品添加购物车失败");

    }


    /**
     * 登录成功后需要整合cookie和redis的数据
     * cookie中的购物车和redis中的购物车进行整合
     * 1、从 cookie 中取出购物车列表对象
     * 2、从 redis 中取出购物车对象
     * 3、将 cookie 中的购物车列表和 redis 中的购物车列表整合（取出 cookie 中的购物车列表，然后添加到 redis 购物车中即可）
     * 5、最终展示的结果以 redis 中的购物车为主
     */
    public R mergeCart(Long userId, List<TbItem> itemList) {
        for (TbItem tbItem : itemList) {
            // 只需要调用登录状态下添加购物车业务处理逻辑即可
            addlogCart(userId, tbItem.getId(), tbItem.getNum());
        }
        return R.ok();
    }

    /**
     * 删除购物车
     *
     * @return
     */
    public R deleteCartItem(Long id, Long itemId) {
        Long hdel = redisTemplate.opsForHash().delete(CART_REDIS_KEY_PRE + ":" + id + "", itemId + "");
        System.out.println("删除购物车购物项为" + hdel);
        return R.ok();
    }

    /**
     * 更新购物车中商品的数量
     */
    public R updateRedisNum(Long id, Long itemId, Integer num) {
        // 取出需要更改数量的商品信息
        Object hget = redisTemplate.opsForHash().get(CART_REDIS_KEY_PRE + ":" + id + "", itemId + "");
        // 将取出的json数据转换为商品对象，然后更新数量
        TbItem item = JSONUtil.toBean(JSONUtil.toJsonStr(hget), TbItem.class);
        item.setNum(num);
        // 更新成功后，将数据写到redis购物车中
        redisTemplate.opsForHash().put(CART_REDIS_KEY_PRE + ":" + id + "", itemId + "", JSONUtil.toJsonStr(item));
        return R.ok();
    }

    private TbItem selectByPrimaryKey(Long itemId) {
        TbItem build = new TbItem();
        build.setBarcode("1");
        build.setTitle("小东西");
        build.setId(itemId);
        return build;
    }

}
