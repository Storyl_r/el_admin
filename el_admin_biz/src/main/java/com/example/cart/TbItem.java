package com.example.cart;

import lombok.Data;

import java.util.Date;

/**
 * @author 购物车
 * @since 2023/3/28 10:44
 */
@Data
public class TbItem {

    private Long id;

    private String title;

    private String sellPoint;

    private Long price;
    /**
     * 作为购物项购买的商品数量
     */
    private Integer num;

    private String barcode;
    /**
     * 展示购物项中的图片
     */
    private String image;

    private Long cid;

    private Byte status;

    private Date created;

    private Date updated;

}
