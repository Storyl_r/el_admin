package com.example.jwt.registry.Interceptor;

import com.example.annotations.Idempotent;
import com.example.api.exception.CommonRuntimeException;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author 接口幂等
 * @since 2023/4/7 9:31
 */
@Component
public class InterfaceOne implements HandlerInterceptor {

    private final ConcurrentHashMap<String, Boolean> idempotentMap = new ConcurrentHashMap<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            if (handlerMethod.hasMethodAnnotation(Idempotent.class)) {
                String idempotentKey = generateIdempotentKey(request);
                if (idempotentMap.containsKey(idempotentKey)) {
                    // 已经处理过该请求，返回 429 Too Many Requests 状态码
                    response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
                    throw new CommonRuntimeException("请求过快，请稍后重试");
                } else {
                    // 第一次处理该请求，放入幂等性校验缓存中
                    idempotentMap.put(idempotentKey, true);
                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // 清除幂等性校验缓存
        idempotentMap.clear();
    }

    private String generateIdempotentKey(HttpServletRequest request) {
        // 根据请求信息生成幂等性校验 key
        return request.getMethod() + "-" + request.getRequestURI() + "-" + request.getParameter("idempotentKey");
    }
}


