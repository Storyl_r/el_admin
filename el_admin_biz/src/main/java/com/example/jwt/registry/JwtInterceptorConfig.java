package com.example.jwt.registry;

//import com.example.jwt.registry.Interceptor.JwtAuthenticationInterceptor;

import com.example.jwt.registry.Interceptor.InterfaceOne;
import com.example.jwt.registry.Interceptor.RequestLimitingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @Author Story
 * @Date 2022/3/20 0:15
 * @Version 1.0
 * 拦截器
 */
@Configuration
public class JwtInterceptorConfig implements WebMvcConfigurer {
    @Autowired
    RequestLimitingInterceptor requestLimitingInterceptor;
    @Autowired
    InterfaceOne interfaceOne;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 可添加多个
        registry.addInterceptor(interfaceOne).addPathPatterns("/**");
        //青少年模式
        registry.addInterceptor(requestLimitingInterceptor).addPathPatterns("/**");
    }


}
