//package com.example.jwt.registry.Interceptor;
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.example.api.entity.biz.User;
//import com.example.api.exception.CommonRuntimeException;
//import com.example.api.vo.biz.UserVo;
//import com.example.jwt.util.JwtUtils;
//import com.example.service.UserService;
//import common.annotations.Inner;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.method.HandlerMethod;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//import utils.BeanUtils;
//import utils.CryptoUtil;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.lang.reflect.Method;
//
///**
// * @Author Story
// * @Date 2022/3/20 0:17
// * @Version 1.0 自定义处理器拦截器 用户登录时判断携带在参数是否合法
// */
//@Slf4j
//public class JwtAuthenticationInterceptor implements HandlerInterceptor {
//
//    @Autowired
//    UserService accountService;
//
//    @Override
//    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) {
//        // 从请求头中取出token（jwt）
//        String token = httpServletRequest.getHeader("token");
//        // 如果不是映射到方法直接通过 因为只需要对方法进行拦截
//        if (!(object instanceof HandlerMethod)) {
//            return true;
//        }
//        HandlerMethod handlerMethod = (HandlerMethod) object;
//        Method method = handlerMethod.getMethod();
//        //检查是否有inner注释，有则跳过认证
//        if (method.isAnnotationPresent(Inner.class)) {
//            Inner inner = method.getAnnotation(Inner.class);
//            if (inner.required()) {
//                return true;
//            }
//        } else {
//            log.info("被jwt拦截需要验证:{}", method.getName());
//            // 执行认证
//            if (token == null) {
//                //这里其实是登录失效
//                throw new CommonRuntimeException("登陆信息失效");
//            }
//
//            String userId = null;
//            try {
//                //进行解密
//                token = CryptoUtil.decodeTarget(token);
//                userId = JwtUtils.getAudience(token);
//            } catch (Exception e) {
//                throw new CommonRuntimeException("获取token中的userName异常");
//            }
//
//            //检查用户是否存在，
//            UserVo user;
//            User byUserId = accountService.getBaseMapper().selectOne(Wrappers.<User>lambdaQuery().eq(User::getUserId, userId).last("limit 1"));
//            User byName = accountService.getBaseMapper().selectOne(Wrappers.<User>lambdaQuery().eq(User::getName, userId).last("limit 1"));
//            if (null != byUserId) {
//                user = BeanUtils.toBean(byUserId, UserVo.class);
//            } else {
//                user = BeanUtils.toBean(byName, UserVo.class);
//            }
//            if (user == null) {
//                throw new CommonRuntimeException("登录信息不存在");
//            }
//            try {
//                JwtUtils.verifyToken(token, userId);
//            } catch (Exception e) {
//                throw new CommonRuntimeException("验证token失败====");
//            }
//
//            //获取载荷内容
//            String userName = JwtUtils.getClaimByName(token, "userName").asString();
//            String realName = JwtUtils.getClaimByName(token, "realName").asString();
//
//            //放入attribute以便后面调用
//            httpServletRequest.setAttribute("userName", userName);
//            httpServletRequest.setAttribute("realName", realName);
//            return true;
//
//        }
//        return true;
//    }
//
//    @Override
//    public void postHandle(HttpServletRequest httpServletRequest,
//                           HttpServletResponse httpServletResponse,
//                           Object o, ModelAndView modelAndView) {
//
//    }
//
//    @Override
//    public void afterCompletion(HttpServletRequest httpServletRequest,
//                                HttpServletResponse httpServletResponse,
//                                Object o, Exception e) {
//    }
//
//}
