package com.example.jwt.util;

import com.example.api.config.OtherConfig;
import com.example.ms.AppConfig;
import com.example.ms.Conditional;
import com.example.ms.ImportClass;
import com.example.service.AccountService;
import com.example.util.MultiplyThreadTransactionManager;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootTest
@Slf4j
@Import({ImportClass.class})
public class JwtUtilsTest {

    @Autowired
    RedisTemplate<String, String> redisTemplate;

    @Autowired
    MultiplyThreadTransactionManager transactionManager;

    @Autowired
    AccountService accountService;

    @Autowired
    OtherConfig otherConfig;

    @Test
    public void aaaa() {
        System.out.println(otherConfig.getStringList());
        System.out.println(otherConfig.getPwc());

    }
    @Test
    public void aaa() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        Conditional orderService = (Conditional) applicationContext.getBean("conditional");
        System.out.println(orderService);
        applicationContext.close();

    }

    @Test
    public void a() {
        List<Runnable> tasks = new ArrayList<>();

        tasks.add(() -> {
            throw new RuntimeException("我就要抛出异常!");
        });
        tasks.add(() -> {
        });
        transactionManager.runAsyncButWaitUntilAllDown(tasks, Executors.newCachedThreadPool());
    }

    @Test
    public void abc() {
        String redisKey = "lock";
        ExecutorService executorService = Executors.newFixedThreadPool(20);//20个线程
        for (int i = 0; i < 1000; i++) {//尝试1000次
            executorService.execute(() -> {
                try {
                    redisTemplate.watch(redisKey);
                    String redisValue = redisTemplate.opsForValue().get(redisKey);
                    int valInteger = Integer.valueOf(redisValue);
                    String userInfo = UUID.randomUUID().toString();
                    // 没有秒完
                    if (valInteger < 20) {//redisKey
                        redisTemplate.multi();//开启事务
                        redisTemplate.opsForValue().increment(redisKey);//自增
                        List list = redisTemplate.exec();//提交事务，如果返回nil则说明执行失败，因为我watch了的，只要执行失败，则
                        // 进来发现东西还有，秒杀成功
                        if (list != null && list.size() > 0) {
                            System.out.println("用户：" + userInfo + "，秒杀成功！当前成功人数：" + (valInteger + 1));
                        } else {//执行结果不是OK，说明被修改了，被别人抢了
                            System.out.println("用户：" + userInfo + "，秒杀失败");
                        }
                    } else {//东西秒完了
                        System.out.println("已经有20人秒杀成功，秒杀结束");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        executorService.shutdown();//关闭线程池
    }
}
