package spring;

import aspect.Autowired;
import aspect.Component;
import aspect.ComponentScan;
import aspect.Scope;
import service.BeanPostProcessor;

import java.beans.Introspector;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplicationContext {
    private Class config;

    private final static Map<String, BeanDefinition> beanDefinitionMap = new HashMap();
    private final static Map<String, Object> singletonObjects = new HashMap();
    private List<BeanPostProcessor> beanPostProcessorList = new ArrayList<>();


    public ApplicationContext(Class config) {
        this.config = config;
        //扫描
        scan(config);
        beanDefinitionMap.forEach((x, v) -> {
            if (v.getScope().equals("singleton")) {
                singletonObjects.put(x, createBean(x, v));
            }
        });
    }

    /**
     * 扫描
     *
     * @param config
     */
    private void scan(Class config) {
        if (config.isAnnotationPresent(ComponentScan.class)) {
            ComponentScan componentScanAnnotation = (ComponentScan) config.getAnnotation(ComponentScan.class);
            String scanPath = componentScanAnnotation.scanPath();
            //获取当前类的加载器
            ClassLoader classLoader = ApplicationContext.class.getClassLoader();
            URL resource = classLoader.getResource(scanPath);
            File file = new File(resource.getFile());

            //判断是否文件
            if (file.isDirectory()) {
                try {
                    for (File f : file.listFiles()) {
                        String absolutePath = f.getAbsolutePath();
                        absolutePath = absolutePath.substring(absolutePath.indexOf("service"), absolutePath.indexOf(".class"));
                        absolutePath = absolutePath.replace("\\", ".");
                        Class<?> aClass = null;
                        aClass = classLoader.loadClass(absolutePath);
                        //如果包含该注解
                        if (aClass.isAnnotationPresent(Component.class)) {
                            //是否实现BeanPostProcessor
                            if (BeanPostProcessor.class.isAssignableFrom(aClass)) {
                                BeanPostProcessor postProcessor = (BeanPostProcessor) aClass.getConstructor().newInstance();
                                beanPostProcessorList.add(postProcessor);
                            }

                            Component component = aClass.getAnnotation(Component.class);
                            //定义的bean名称
                            String beanName = component.name();
                            //没有指定给默认
                            if ("".equals(beanName)) {
                                beanName = Introspector.decapitalize(aClass.getSimpleName());
                            }

                            BeanDefinition definition = new BeanDefinition();
                            if (aClass.isAnnotationPresent(Scope.class)) {
                                Scope annotation = aClass.getAnnotation(Scope.class);
                                String name = annotation.name();
                                definition.setScope(name);
                                definition.setType(aClass);
                                if (name.equals("")) {
                                    definition.setScope("singleton");
                                }
                            } else {
                                definition.setScope("prototype");
                                definition.setType(aClass);
                            }
                            beanDefinitionMap.put(beanName, definition);
                        } else {
                            //不是bean不处理
                        }
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * 获取bean
     *
     * @param beanName
     * @return
     */
    public Object getBean(String beanName) {
        Object bean = null;
        if (!beanDefinitionMap.containsKey(beanName)) {
            throw new NullPointerException("未找到对应的bean");
        }
        BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
        if (beanDefinition.getScope().equals("singleton")) {
            //从单列map取
            bean = singletonObjects.get(beanName);
            if (null == bean) {
                bean = createBean(beanName, beanDefinition);
                singletonObjects.put(beanName, bean);
            }

        } else {
            //原型bean，每次拿都是创建新的对象
            bean = createBean(beanName, beanDefinition);
        }
        return bean;
    }

    /**
     * 创建bean
     *
     * @param beanName
     * @param beanDefinition
     * @return
     */
    private Object createBean(String beanName, BeanDefinition beanDefinition) {
        Class aClass = beanDefinition.getType();
        Object newInstance = null;
        try {
            newInstance = aClass.getConstructor().newInstance();
            //属性注入
            for (Field field : aClass.getDeclaredFields()) {
                if (field.isAnnotationPresent(Autowired.class)) {
                    field.setAccessible(true);
                    Object bean = getBean(field.getName());
                    field.set(newInstance, bean);
                }
            }

            // BeanPostProcessor 前置处理
            for (BeanPostProcessor postProcessor : beanPostProcessorList) {
                newInstance = postProcessor.befor(newInstance, beanName);
            }

            //执行初始化bean操作
            if (newInstance instanceof InitializingBean) {
                ((InitializingBean) newInstance).afterPropertiesSet();
            }

            //bean初始化后操作
            for (BeanPostProcessor postProcessor : beanPostProcessorList) {
                newInstance = postProcessor.after(newInstance, beanName);
            }


        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return newInstance;
    }
}
