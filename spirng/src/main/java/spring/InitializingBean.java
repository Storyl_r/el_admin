package spring;

/**
 * 初始化接口
 */
public interface InitializingBean {
   void afterPropertiesSet();
}
