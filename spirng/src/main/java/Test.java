import service.UserInterface;
import spring.ApplicationContext;

public class Test {
    public static void main(String[] args) {
        //创建单列bean
        ApplicationContext context = new ApplicationContext(AppConfig.class);
        UserInterface userService = (UserInterface) context.getBean("userService");
        System.out.println("userService 类型: " + userService.getClass().getName());
        userService.test();

    }
}
