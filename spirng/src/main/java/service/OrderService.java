package service;

import aspect.Component;
import aspect.Scope;
import spring.InitializingBean;

@Component(name = "orderService")
@Scope(name = "singleton")
public class OrderService implements InitializingBean {
    /*@Autowired
    private UserInterface userService;*/

    @Override
    public void afterPropertiesSet() {
        System.out.println("bean初始化:afterPropertiesSet");
    }


}
