package service;


public interface BeanPostProcessor {

    default Object befor(Object bean, String beanName) {
        return bean;
    }

    default Object after(Object bean, String beanName) {
        return bean;
    }

}
