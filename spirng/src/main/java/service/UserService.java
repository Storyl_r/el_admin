package service;

import aspect.Component;
import aspect.Scope;

@Component(name = "userService")
@Scope(name = "singleton")
public class UserService implements UserInterface {
    public void test() {
    }
}
