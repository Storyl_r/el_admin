package service;

import aspect.Component;
import aspect.MyValue;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object befor(Object bean, String beanName) {
        for (Field declaredField : bean.getClass().getDeclaredFields()) {
            if (declaredField.isAnnotationPresent(MyValue.class)) {
                declaredField.setAccessible(true);
                try {
                    declaredField.set(bean, declaredField.getAnnotation(MyValue.class).value());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return bean;
    }
    @Override
    public Object after(Object bean, String beanName) {
        if ("userService".equals(beanName)) {
            return Proxy.newProxyInstance(
                    MyBeanPostProcessor.class.getClassLoader(),
                    bean.getClass().getInterfaces(),
                    new InvocationHandler() {
                        @Override
                        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                            System.out.println("AOP生产代理对象");
                            return method.invoke(bean, args);
                        }
                    }
            );
        }
        return bean;
    }

}
