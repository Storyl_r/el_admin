package com.example.api.config;


/**
 * @author Administrator
 */

public enum ConfigEnum {

    SQL_WARN_TIME("code","100" );
    private final String status;
    private final String value;

    ConfigEnum(String status, String value) {
        this.status = status;
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public String getValue() {
        return value;
    }
}
