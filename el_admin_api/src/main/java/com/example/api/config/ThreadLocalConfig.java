package com.example.api.config;

public class ThreadLocalConfig {

    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static String getThreadLocal() {
        return threadLocal.get();
    }

    public static void setThreadLocal() {
        threadLocal.set("thread");
    }
}
