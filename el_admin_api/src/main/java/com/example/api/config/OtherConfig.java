package com.example.api.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

/**
 * 用于配置一些公共的属性
 *
 * @author lr
 * @create 2021/10/15
 */
@Data
@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "other")
public class OtherConfig {


    /**
     * 队列名称
     */
    private String queue;

    /**
     * 消息标识
     */
    private String projectMessage;
    /**
     * 消息交换机
     */
    private String exchange;

    /**
     * 消息交换机
     */
    private List<String> stringList;

    private Map<String, Map<String, String>> pwc;


}