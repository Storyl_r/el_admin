package com.example.api.constants;

public interface CacheConstant {
    String GLOBALLY = "gl:";
    String DEFAULT_REGION_KEY = "region_tree";
    String DICTIONARY_TREE = "dictionary_tree";
    String USER_TREE="user_tree_";
}