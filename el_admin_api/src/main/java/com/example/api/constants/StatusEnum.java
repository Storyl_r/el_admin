package com.example.api.constants;

import cn.hutool.core.util.ObjectUtil;

/**
 * 状态枚举类，数据字典（ht_form_status）
 *
 * @author hualin
 * @date 2021-04-02 09:11:19
 **/
public enum StatusEnum {

    ADDED("0", "新增"),
    ENABLE("1", "启用"),
    DISABLE("2", "停用"),
    TEMP_STORAGE("3", "暂存"),
    IN_APPROVAL("4", "审批中"),
    REJECTION_APPROVAL("5", "审批驳回"),
    INVALID("6", "已作废"),
    PASS("7", "审批通过"),
    PROCESSING("8", "进行中");

    StatusEnum(String status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    private final String status;
    private final String desc;

    public String getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static String getDesc(String status) {
        if (ObjectUtil.isEmpty(status)) {
            return null;
        }
        for (StatusEnum statusEnum : StatusEnum.values()) {
            if (statusEnum.getStatus().equals(status)) {
                return statusEnum.getDesc();
            }
        }
        return null;
    }

    public static boolean contains(String status) {
        if (ObjectUtil.isEmpty(status)) {
            return false;
        }
        for (StatusEnum e : StatusEnum.values()) {
            if (e.getStatus().equals(status)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public String toString() {
        return status;
    }

}