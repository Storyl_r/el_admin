package com.example.api.constants;

public interface ModuleConstant {
    String BUDGET_ACCOUNT_CONFIG = "budget_account_config";
    String SETTLE_ACCOUNT_CONFIG = "settle_account_config";
    String BUDGET_ACCOUNT_RULE = "budget_account_rule";
    String SETTLE_ACCOUNT_RULE = "settle_account_rule";
    String CONSULTS_CHARGE = "consults_charge";
    String GOVERNMENT_FILE = "government_file";
}