package com.example.api.constants;

/**
 * API常量
 *
 * @author: OprCalf
 * @date: 2020年5月2日
 */
public class ApiConstant {

    /*********************************swagger作者常量****************************************/

    public static final String SW_AUTHOR = "林溪";

    public static final String SW_EMAIL = "localhost@126.com";

    public static final String SW_URL = "localhost@126.com";

    public static final String SW_VERSION = "1.0.0";

    /*********************************swagger所有服务常量****************************************/

    public static final String SW_PACKPATH_ALL = "net.herdao";

    public static final String SW_TITLE_ALL = "所有服务";

    public static final String SW_DESCRIPTION_ALL = "所有服务";

    /*********************************swagger后台服务接口常量****************************************/

    public static final String SW_PACKPATH_VIEWS = "net.herdao";

    public static final String SW_TITLE_VIEWS = "viewapis";

    public static final String SW_DESCRIPTION_VIEWS = "后台服务接口";

    /*********************************swagger小程序端接口常量****************************************/

    public static final String SW_PACKPATH_APPLICATIONS = "net.herdao";

    public static final String SW_TITLE_APPLICATIONS = "appapis";

    public static final String SW_DESCRIPTION_APPLICATIONS = "小程序端服务接口";

    /*********************************swagger第三方接口常量****************************************/

    public static final String SW_PACKPATH_EXTERNALS = "net.herdao";

    public static final String SW_TITLE_EXTERNALS = "externalapis";

    public static final String SW_DESCRIPTION_EXTERNALS = "第三方服务接口";

    /*********************************swagger手机端接口常量****************************************/

    public static final String SW_PACKPATH_MOBILES = "net.herdao";

    public static final String SW_TITLE_MOBILES = "mobileapis";

    public static final String SW_DESCRIPTION_MOBILES = "手机端服务接口";

    /*********************************swagger H5端接口常量****************************************/

    public static final String SW_PACKPATH_H5 = "net.herdao";

    public static final String SW_TITLE_H5 = "h5apis";

    public static final String SW_DESCRIPTION_H5 = "H5端服务接口";

}
