package com.example.api.annotations.dict;

import java.lang.annotation.*;

/**
 * @Author: lr
 * 需要引入DictAspect.java拦截器往Controller的TableDataInfo和AjaxResult返回结果的实体注入字典翻译
 * @Description:
 * @Date: 20220217
 */

@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Dict {

    String dictType() default "";

    int getCodeOrName() default 0;

}