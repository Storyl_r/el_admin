package com.example.api.common;

/**
 * 常用状态
 */
public interface CommonConstants {
    String TENANT_ID = "TENANT-ID";
    String LOCALE = "I18N_LOCALE";
    String I18N_LOCALE_ZH_CN = "zh_CN";
    String I18N_LOCALE_EN_US = "en_US";
    String VERSION = "VERSION";
    Integer TENANT_ID_1 = 1;
    String STATUS_DEL = "1";
    String STATUS_NORMAL = "0";
    String STATUS_LOCK = "9";
    String SYS_LOG_TYPE_NAME = "sysLogTypeName";
    String SYS_LOG_TYPE_VALUE_API = "接口日志";
    String SYS_LOG_TYPE_VALUE_LOGIN_AUTH = "登录认证";
    String SYS_LOG_TYPE_TITLE = "sysLogTypeTitle";
    String SYS_LOG_REQUEST_URI = "request_uri";
    String SYS_LOG_SERVICE_ID = "client_id";
    String SYS_LOG_USER_NAME = "username";
    String SYS_LOG_EXECUTION_TIME = "execution_time";
    String SYS_LOG_SYS_LOG = "sys_log";
    String SYS_LOG_TENANT_ID = "tenant_id";
    Integer MENU_TREE_ROOT_ID = -1;
    Long ORG_TREE_ROOT_ID = 0L;
    String UTF8 = "UTF-8";
    String FRONT_END_PROJECT = "hdp-ui";
    String BACK_END_PROJECT = "hdp";
    String HDP_PUBLIC_PARAM_KEY = "HDP_PUBLIC_PARAM_KEY";
    Integer SUCCESS = 0;
    Integer FAIL = 1;
    String BUCKET_NAME = "hdp";
    String IMAGE_CODE_TYPE = "blockPuzzle";
    String AUTO_CONFIG = "0";
    String MANUAL_CONFIG = "1";
    String USER_SOURCE = "u";
    String ORG_SOURCE = "o";
    String MAIL_MSG = "1";
    String WX_MSG = "2";
    String QYWX_MSG = "3";
    String USE_FLAG_START = "0";
    String USE_FLAG_STOP = "1";
    String YZJ_APP = "app";
    String YZJ_TEAM = "team";
    String YZJ_RES_GROUP_SECRET = "resGroupSecret";
    String HDP_PRODUCT_CODE = "hdp";
    String ADMIN_USER = "admin";
    String KEY_ALL = "all";
}
