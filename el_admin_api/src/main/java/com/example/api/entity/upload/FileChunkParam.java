package com.example.api.entity.upload;

import lombok.*;
import org.springframework.data.annotation.Transient;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class FileChunkParam {
    /**
     * 文件名
     */
    private String id;
    /**
     * 文件名
     */
    private String fileName;

    /**
     * 当前分片，从1开始
     */
    private Integer chunkNumber;

    /**
     * 分片大小
     */
    private Float chunkSize;

    /**
     * 当前分片大小
     */
    private Float currentChunkSize;

    /**
     * 文件总大小
     */
    private Double totalSize;

    /**
     * 总分片数
     */
    private Integer totalChunk;

    /**
     * 文件标识
     */
    private String identifier;

    /**
     * md5校验码
     */
    private String relativePath;

    /**
     * createtime
     */
    private Date createtime;

    /**
     * updatetime
     */
    private Date updatetime;

    @Transient
    private MultipartFile file;
}
