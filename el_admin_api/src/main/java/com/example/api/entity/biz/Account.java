package com.example.api.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.api.entity.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
/**
 * @description 账号
 * @author lr
 * @date 2022-03-18
 */
@Data
@EqualsAndHashCode
@TableName("sys_account_t")
public class Account extends BaseEntity<Account> implements  Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 用户id
     */
    private String userId;

    /**
     * 登录账号，如手机号等
     */
    private String openCode;

    /**
     * 账号类别
     */
    private int category;

    /**
     * 逻辑删除:0=未删除，1=已删除
     */
    private Double deleted;



}