package com.example.api.entity.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhengkai.blog.csdn.net
 * @description 字典项
 * @date 2023-02-17
 */
@Data
@TableName("t_sys_dict")
public class SysDict implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * 字典项id
     */
    private String id;

    /**
     * 字典项编码
     */
    private String code;

    /**
     * 字典类型id
     */
    private String dictTypeId;

    /**
     * 字典项名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;
}