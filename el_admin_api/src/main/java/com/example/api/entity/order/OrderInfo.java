package com.example.api.entity.order;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.api.entity.common.BaseIdEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * </p>
 *
 * @author lr
 * @since 2021-04-17
 */
@TableName("order_info")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class OrderInfo extends BaseIdEntity<OrderInfo> {

    private String orderId;

    private String orderType;
}
