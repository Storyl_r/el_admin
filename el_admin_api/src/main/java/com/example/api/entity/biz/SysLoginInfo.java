package com.example.api.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.api.entity.common.BaseIdEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * <p>
 * 用户登陆信息
 * </p>
 *
 * @author lr
 * @since 2022-04-03
 */
@Data
@EqualsAndHashCode
@TableName("sys_login_info_t")
public class SysLoginInfo extends BaseIdEntity<SysLoginInfo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户状态:0=正常，1=禁用")
    private String state;

    @ApiModelProperty(value = "登录人用户id")
    private String userId;

    @ApiModelProperty(value = "登录人姓名")
    private String name;

    @ApiModelProperty(value = "登录人手机号")
    private String mobile;

    @ApiModelProperty(value = "password")
    private String password;

    @ApiModelProperty(value = "逻辑删除:0=未删除，1=已删除")
    private String deleted;

    @ApiModelProperty(value = "登录人工号")
    private String loginNo;

    @ApiModelProperty(value = "登陆时间")
    private LocalDateTime loginTime;


}
