package com.example.api.entity.common;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 实体类父类
 * @author lilq
 * @date 2020-11-11 02:11:41
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class BaseIdEntity<T extends BaseIdEntity> extends Model {

    private static final long serialVersionUID = 8057249767366055482L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

}
