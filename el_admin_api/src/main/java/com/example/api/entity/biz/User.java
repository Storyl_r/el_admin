package com.example.api.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.api.annotations.dict.Dict;
import com.example.api.annotations.dict.DictObj;
import com.example.api.entity.common.BaseEntity;
import lombok.Data;

/**
 * @Author Story
 * @Date 2022/3/19 23:18
 * @Version 1.0
 */
@Data
@TableName("sys_user_t")
public class User extends BaseEntity<User> {

    /**
     * 用户状态:0=正常，1=禁用
     */
    @Dict(dictType = "gender")
    @DictObj
    private String state;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 头像图片地址
     */
    private String headImgUrl;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 密码加盐
     */
    private String salt;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 逻辑删除:0=未删除，1=已删除
     */
    private int deleted;

    /**
     * 版本
     */
    private int version;


    public User(String name, String headImgUrl) {
        this.name = name;
        this.headImgUrl = headImgUrl;
    }

    public User() {
    }
}
