package com.example.api.entity.upload;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.api.entity.common.BaseIdEntity;
import lombok.*;

import java.util.Date;

@TableName("file_chunk")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class FileChunk extends BaseIdEntity<FileChunk> {

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 当前分片，从1开始
     */
    private Integer chunkNumber;

    /**
     * 分片大小
     */
    private Float chunkSize;

    /**
     * 当前分片大小
     */
    private Float currentChunkSize;

    /**
     * 文件总大小
     */
    private Double totalSize;

    /**
     * 总分片数
     */
    private Integer totalChunk;

    /**
     * 文件标识
     */
    private String identifier;

    /**
     * md5校验码
     */
    private String relativePath;

    /**
     * createtime
     */
    private Date createtime;

    /**
     * updatetime
     */
    private Date updatetime;
}
