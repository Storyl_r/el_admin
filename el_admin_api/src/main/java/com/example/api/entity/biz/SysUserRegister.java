package com.example.api.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.api.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户注册信息
 * </p>
 *
 * @author lr
 * @since 2022-03-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "SysUserRegister对象", description = "用户注册信息")
@TableName("sys_user_register_t")
public class SysUserRegister extends BaseEntity<SysUserRegister> implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "用户状态:0=正常，1=禁用")
    private String state;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "注册时间")
    private LocalDateTime registerDate;

    @ApiModelProperty(value = "注册ip")
    private String registerIp;


}
