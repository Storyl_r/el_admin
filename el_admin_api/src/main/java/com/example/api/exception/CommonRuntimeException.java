/*
 * All rights Reserved, Designed By HerDao
 * Copyright:    Copyright(C) 2020-2099
 * Company:      HerDao Ltd.
 */

package com.example.api.exception;

import org.apache.logging.log4j.util.Strings;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 全局公共异常类
 *
 * @author：OprCalf
 * @date：2021年6月9日 上午11:47:27
 */
@Slf4j
public class CommonRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    @Setter
    @Getter
    private String params;

    public CommonRuntimeException(Throwable e, String params) {
        super(e);
        this.params = params;
    }

    public CommonRuntimeException(String params) {
        this(null, params);
    }

    public CommonRuntimeException() {
    }

    @Override
    public String getMessage() {
        log.error("获取到的错误信息:{}", super.getMessage());
        return Strings.isEmpty(params) ? super.getMessage() : params;
    }

    /**
     * 重写fillInStackTrace 业务异常不需要堆栈信息，提高效率.
     *
     * @return
     * @author OprCalf
     */
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

}
