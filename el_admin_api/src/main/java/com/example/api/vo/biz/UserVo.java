package com.example.api.vo.biz;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.example.api.annotations.dict.Dict;
import lombok.Data;

/**
 * @Author Story
 * @Date 2022/3/19 23:18
 * @Version 1.0
 */
@Data
@ColumnWidth(value=80)
public class UserVo {

    private String id;
    /**
     * 用户状态:0=正常，1=禁用
     */
    @ExcelProperty(value = "用户状态", index = 0)
    @Dict(dictType = "state")
    private String state;

    /**
     * 用户id
     */
    @ExcelProperty(value = "用户id", index = 1)
    private String userId;

    /**
     * 姓名
     */
    @ExcelProperty(value = "姓名", index = 2)
    private String name;

    /**
     * 头像图片地址
     */
    @ExcelProperty(value = "头像图片地址", index = 3)
    private String headImgUrl;

    /**
     * 手机号码
     */
    @ExcelProperty(value = "手机号码", index = 4)
    private String mobile;

    /**
     * 密码加盐
     */
    @ExcelProperty(value = "密码加盐", index = 5)
    private String salt;

    /**
     * 登录密码
     */
    @ExcelProperty(value = "登录密码", index = 6)
    private String password;

    /**
     * 逻辑删除:0=未删除，1=已删除
     */
    @ExcelProperty(value = "逻辑删除", index = 7)
    private int deleted;
    /**
     * token认证
     */
    @ExcelProperty(value = "token认证", index = 8)
//    @DateTimeFormat("yyyy-MM-dd")
    private String token;
}
