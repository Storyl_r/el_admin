package com.example.api.vo.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * </p>
 *
 * @author lr
 * @since 2021-04-17
 */
@TableName("order_info")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class OrderInfoVo implements Serializable
{

    private static final long serialVersionUID = 1L;

    private String id;

    private String orderId;

    private String orderType;
}
