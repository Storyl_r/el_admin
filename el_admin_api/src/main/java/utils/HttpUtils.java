package utils;

/**
 * @author
 * @since 2023/4/3 15:12
 */

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HttpUtils {

    /**
     * 发送 HTTP GET 请求
     *
     * @param url    请求的 URL
     * @param params 请求的参数
     * @return 响应内容
     */
    public static String doGet(String url, Map<String, String> params) throws URISyntaxException, IOException {
        HttpClient httpClient = HttpClients.createDefault();
        URIBuilder uriBuilder = new URIBuilder(url);
        if (params != null) {
            // 将参数加入 URI
            for (Map.Entry<String, String> entry : params.entrySet()) {
                uriBuilder.setParameter(entry.getKey(), entry.getValue());
            }
        }
        HttpGet httpGet = new HttpGet(uriBuilder.build());
        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        String response = EntityUtils.toString(httpEntity);
        return response;
    }

    /**
     * 发送 HTTP POST 请求
     *
     * @param url    请求的 URL
     * @param params 请求的参数
     * @return 响应内容
     */
    public static String doPost(String url, Map<String, String> params) throws UnsupportedEncodingException, IOException {
        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        if (params != null) {
            // 添加表单参数
            httpPost.setEntity(new UrlEncodedFormEntity(getFormParams(params), StandardCharsets.UTF_8));
        }
        HttpResponse httpResponse = httpClient.execute(httpPost);
        HttpEntity httpEntity = httpResponse.getEntity();
        String response = EntityUtils.toString(httpEntity);
        return response;
    }

    /**
     * 将 Map 转换为表单参数
     *
     * @param params 参数 Map
     * @return 表单参数
     */
    private static Iterable<? extends NameValuePair> getFormParams(Map<String, String> params) {
        if (params == null) {
            return null;
        }
        Map<String, String> formParams = new HashMap<>(params);
        // 对参数值进行 URL 编码
        for (Map.Entry<String, String> entry : params.entrySet()) {
            formParams.put(entry.getKey(), urlEncode(entry.getValue()));
        }
        List<NameValuePair> list = new ArrayList<>();
        // 将 Map 转换为表单参数
        for (Map.Entry<String, String> entry : formParams.entrySet()) {
            list.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
        return list;
    }

    /**
     * 对字符串进行 URL 编码
     *
     * @param value 待编码的字符串
     * @return 编码后的字符串
     */
    private static String urlEncode(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("UTF-8 encoding not supported");
        }
    }
}
