package utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.StrUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lr
 * @create 2021/4/1
 */
public class BeanUtils {

    public BeanUtils() {
    }

    public static <T> T toBean(Object source, Class<T> clazz) {
        return BeanUtil.toBean(source, clazz, (CopyOptions) null);
    }

    public static <T, E> List<T> toBeanList(List<E> sources, Class<T> clazz) {
        List<T> targets = new ArrayList();
        sources.forEach((source) -> {
            T target = BeanUtil.toBean(source, clazz, (CopyOptions) null);
            targets.add(target);
        });
        return targets;
    }

    /**
     * 判断对象中属性值是否全为空
     *
     * @param object
     * @return
     */
    public static boolean checkObjAllFieldsIsNull(Object object) {
        if (null == object) {
            return true;
        }

        try {
            for (Field f : object.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                Object  v = f.get(object);
                if (!f.getName().equals("serialVersionUID") &&null!= v  && StrUtil.isNotBlank(f.get(object).toString())) {
                    return false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

}