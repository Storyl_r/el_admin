package utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.*;

/**
 * @类说明 时间类
 * @create 2012-11-29 下午2:42:19
 */
public class DateUtils {

    public static final String FORMAT_0 = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_1 = "yyyy-MM-dd";
    public static final String FORMAT_2 = "HH:mm:ss";
    public static final String FORMAT_3 = "MM/dd/yyyy HH:mm:ss";
    public static final String FORMAT_4 = "yyyyddMMHHmmss";
    public static final String FORMAT_5 = "yyyy-MM";
    public static final String FORMAT_6 = "yyMMddHHmmssSSS";
    public static final String FORMAT_7 = "yyyyMMddHHmmss";
    public static final String FORMAT_8 = "yyyyMMddHHmmssSSS";
    public static final String FORMAT_9 = "yyyyMMdd";
    public static final String PERMANENT_DATE = "2099-12-31 23:59:59";
    public static final Long PERMANENT_DATE_LONG = 4102415999L; // 2099-12-31
    // 23:59:59
    public static final Integer SPRING = 1;
    public static final Integer SUMMER = 2;
    public static final Integer AUTUMN = 3;
    public static final Integer WINTER = 4;


    public static final Integer getSeasonByMonth(int month) {
        if (month >= 1 && month <= 3) {
            return SPRING;
        } else if (month >= 4 && month <= 6) {
            return SUMMER;
        } else if (month >= 7 && month <= 9) {
            return AUTUMN;
        } else if (month >= 10 && month <= 12) {
            return WINTER;
        }
        return null;
    }

    public static final synchronized String getFormatDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyMMddHHmmss");
        return format.format(new Date());
    }

    public static final synchronized String getFormatDate2Str(String format) {
        SimpleDateFormat formatDate = new SimpleDateFormat(format);
        return formatDate.format(new Date());
    }

    public static final synchronized Date getFormatDate(String fromat) {
        try {
            DateFormat format = new SimpleDateFormat(fromat);

            Date date = format.parse(format.format(new Date()));
            return date;
        } catch (Exception e) {

        }
        return null;

    }

    public static final synchronized Date getFormatDate(Date date, String fromat) throws ParseException {
        try {
            DateFormat format = new SimpleDateFormat(fromat);
            return format.parse(format.format(date));
        } catch (ParseException e) {
            throw e;
        }
    }

    public static final synchronized String getMsecFormatDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyMMddHHmmssSSS");
        return format.format(new Date());
    }

    public static final synchronized String getSimpleFormatDate() {
        SimpleDateFormat format = new SimpleDateFormat("mmss");
        return format.format(new Date());
    }

    public static final Date parseDate(String dateStr) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyMMddHHmmss");
        return format.parse(dateStr);
    }

    public static final Date parseDate(String dateStr, String pattern) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.parse(dateStr);
    }

    public static final Date parseDateForIvm(String dateStr) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
        return format.parse(dateStr);
    }

    /**
     * 格式化
     *
     * @param dataStr
     * @return
     */
    public static Date format4Date(String dataStr) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_3);
        try {
            return sdf.parse(dataStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 格式化
     *
     * @param date
     * @return
     */
    public static String format(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_0);
        return sdf.format(date);
    }

    public static String format(Date date,String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * 格式化
     *
     * @param date
     * @return
     */
    public static String format(LocalDateTime date) {
        if (date == null) {
            return null;
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern(FORMAT_0);
        return df.format(date);
    }

    /**
     * 格式化
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String format(LocalDateTime date, String pattern) {
        if (date == null) {
            return null;
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern(pattern);
        return df.format(date);
    }

    /**
     * 格式化
     * @param date
     * @return
     */
    public static String format(LocalDate date) {
        if (date == null) {
            return null;
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return df.format(date);
    }

    /**
     *
     * @param date
     * @param format
     * @return
     */
    public static String format(LocalDate date,String format) {
        if (date == null) {
            return null;
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern(format);
        return df.format(date);
    }


    /**
     * 格式化
     *
     * @return
     */
    public static String format(long timestamp) {
        String str = String.valueOf(timestamp);
        if (str.length() == 10) {
            timestamp = timestamp * 1000;
        }
        Date date = new Date(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_1);
        return sdf.format(date);
    }

    /**
     * 格式化(yyyy-MM-dd HH:mm:ss)
     *
     * @return
     */
    public static String format(long timestamp, String format) {
        String str = String.valueOf(timestamp);
        if (str.length() == 10) {
            timestamp = timestamp * 1000;
        }
        Date date = new Date(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * String型转date
     *
     * @param date
     * @param parrent
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String getStringDate(Date date, String parrent) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(parrent);
        try {
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 当前时间转字符串
     *
     * @author lilq
     * @date 2021-01-13 09:01:34
     **/
    public static String getCurrentStringDate(String parrent) {
        SimpleDateFormat sdf = new SimpleDateFormat(parrent);
        try {
            return sdf.format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据UTC 方式进行转换 传出单数的格式:20130502T03:32:11Z
     *
     * @param timeStr
     * @return
     */
    public static Date getTimeZone(String timeStr) {
        timeStr = timeStr.replace("Z", " UTC");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss Z");
        try {
            sdf.parse(timeStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c = sdf.getCalendar();
        return c.getTime();
    }

    /**
     * utc时间转换为本地时间
     *
     * @param utcTime eg：20130502T03:32:11Z
     * @return 本地时间Date
     */
    public static Date utc2Local(String utcTime) {
        String utcTimePatten = "yyyyMMdd'T'HH:mm:ss'Z'";
        SimpleDateFormat utcFormater = new SimpleDateFormat(utcTimePatten);
        utcFormater.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date gpsUTCDate = null;
        try {
            gpsUTCDate = utcFormater.parse(utcTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return gpsUTCDate;
    }

    public static Date getTime(String timeStr, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date t = null;
        try {
            t = sdf.parse(timeStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     * 按毫秒数获取天数、小时、分钟、秒
     *
     * @param time
     * @return
     */
    public static String formatByTime(long time) {
        long diff = time / 1000;
        long _m = 60;
        long _h = 60 * _m;
        long _d = 24 * _h;

        long day = diff / _d;
        long h = diff % _d / _h;
        long m = diff % _d % _h / _m;
        long s = diff % _d % _h % _m;
        return day + "天" + h + "小时" + m + "分钟" + s + "秒";
    }

    /**
     * 根据字符串格式去转换相应格式的日期和时间
     *
     * @return java.util.Date
     * @throws ParseException 如果参数格式不正确会抛出此异常
     **/
    public static Date string2Date(String date) {
        SimpleDateFormat simple = null;
        switch (date.trim().length()) {
            case 19:// 日期+时间
                simple = new SimpleDateFormat(FORMAT_0);
                break;
            case 10:// 仅日期
                simple = new SimpleDateFormat(FORMAT_1);
                break;
            case 8:// 仅时间
                simple = new SimpleDateFormat(FORMAT_2);
                break;
            default:
                break;
        }
        try {
            return simple.parse(date.trim());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 比较两个时间大小
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isAfter(String date1, String date2) {
        if (StrUtil.isBlank(date1)) {
            return false;
        }
        if (StrUtil.isBlank(date2)) {
            return true;
        }
        Date date_1 = string2Date(date1);
        Date date_2 = string2Date(date2);
        if (date_1.getTime() > date_2.getTime()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 比较两个时间大小
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isAfter(LocalDateTime date1, LocalDateTime date2) {
        if (date1 == null) {
            return false;
        }
        if (date2 == null) {
            return true;
        }
        if (date1.isAfter(date2)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 比较两个日期的时间差 毫秒数
     *
     * @param date1
     * @param date2
     * @return
     */
    public static Long diffDate(Date date1, Date date2) {
        long diff = date1.getTime() - date2.getTime();
        return diff;

    }

    public static boolean compareDate(Date date1, Date date2) throws Exception {

        if (date1.getTime() >= date2.getTime()) {

            return true;
        } else {
            return false;
        }
    }

    public static String addHour(String dateStr, int hour) throws Exception {
        Date date = parseDate(dateStr);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hour);

        return DateUtils.getStringDate(cal.getTime(), DateUtils.FORMAT_0);
    }

    public static String addHour(Date date, int hour) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.add(Calendar.HOUR, hour);
        return DateUtils.getStringDate(cal.getTime(), DateUtils.FORMAT_0);
    }

    public static String addDate(LocalDateTime time, int date) {
        if (time == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtils.getDateOfDateTime(time));
        cal.add(Calendar.DATE, date);
        return DateUtils.getStringDate(cal.getTime(), DateUtils.FORMAT_0);
    }

    /**
     * 获取当前月份的天数
     *
     * @param date
     * @return
     */
    public static int days4Month(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);

    }

    /**
     * 获取一整年的天数
     *
     * @param date
     * @return
     */
    public static int days4Year(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.getActualMaximum(Calendar.DAY_OF_YEAR);

    }

    /**
     * 获取当前时间的所在季度天数
     *
     * @param date
     * @return
     */
    public static int days4Quarter(Date date) {
        Calendar cal = Calendar.getInstance();
        int days = 0;
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        if (month >= 0 && month < 3) {// 第一季度
            cal.set(Calendar.MONTH, 0);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.MONTH, 1);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.MONTH, 2);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        } else if (month >= 3 && month < 6) {// 第二季度
            cal.set(Calendar.MONTH, 3);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.MONTH, 4);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.MONTH, 5);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        } else if (month >= 6 && month < 8) {// 第三季度
            cal.set(Calendar.MONTH, 6);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.MONTH, 7);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.MONTH, 8);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        } else {
            cal.set(Calendar.MONTH, 9);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.MONTH, 10);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.MONTH, 11);
            days += cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        }
        return days;

    }

    /**
     * 计算剩余时间 以几年几月几日形式返回
     *
     * @param stateDate
     * @param endDate
     * @return
     */
    public static String remainDateToString(Date stateDate, Date endDate) {
        String startDateStr = getStringDate(stateDate, FORMAT_1);
        String endDateStr = getStringDate(endDate, FORMAT_1);
        return remainDateToString(startDateStr, endDateStr);
    }

    /**
     * 计算剩余时间 以几年几月几日形式返回
     *
     * @param startDateStr
     * @param endDateStr
     * @return
     */
    public static String remainDateToString(String startDateStr, String endDateStr) {

        StringBuilder sBuilder = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = new SimpleDateFormat(FORMAT_1).parse(startDateStr);
            endDate = new SimpleDateFormat(FORMAT_1).parse(endDateStr);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        if (endDate.compareTo(startDate) < 0) {
            return "";
        }
        cal.setTime(startDate);
        int startY = cal.get(Calendar.YEAR);
        int startM = cal.get(Calendar.MONTH);
        int startD = cal.get(Calendar.DATE);
        int startDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        cal.setTime(endDate);
        int endY = cal.get(Calendar.YEAR);
        int endM = cal.get(Calendar.MONTH);
        // 处理2011-01-10到2011-01-10，认为服务为一天
        int endD = cal.get(Calendar.DATE) + 1;
        int endDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        int lday = endD - startD;
        if (lday < 0) {
            endM = endM - 1;
            lday = startDayOfMonth + lday;
        }
        // 处理天数问题，如：2011-01-01 到 2013-12-31 2年11个月31天 实际上就是3年
        if (lday == endDayOfMonth) {
            endM = endM + 1;
            lday = 0;
        }
        int mos = (endY - startY) * 12 + (endM - startM);
        int lyear = mos / 12;
        int lmonth = mos % 12;
        if (lyear > 0) {
            sBuilder.append(lyear + "年");
        } else {
            sBuilder.append(0 + "年");
        }
        if (lmonth > 0) {
            sBuilder.append(lmonth + "月");
        } else {
            sBuilder.append(0 + "月");
        }

        // 假如是小于一岁的时候，
        if (lyear == 0) {
            // sBuilder.append(lday+"天");
            if (lday > 0) {
                sBuilder.append(lday - 1 + "天");
            }
        } else {
            if (lday > 0) {
                sBuilder.append(lday + "天");
            } else {
                sBuilder.append(0 + "天");
            }
        }
        return sBuilder.toString();
    }

    /**
     * 截取日期的年月日
     *
     * @param date
     * @return
     */
    public static String subTimeForDate(Date date) {
        String resultStr = "";
        if (date != null) {
            resultStr = date.toString();
            int index = resultStr.indexOf(" ");
            resultStr = resultStr.substring(0, index);
        }
        return resultStr;
    }

    /**
     * 是否为允许时间
     *
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public static boolean isAllowDate(Date beginTime, Date endTime) throws Exception {
        boolean result = false;
        if (beginTime != null && endTime != null) {
            Date now = new Date();
            // 当前时间介乎于开始和结束时间之间，则视为允许过期
            if (compareDate(now, beginTime) && compareDate(endTime, now)) {
                result = true;
            }
        }
        return result;
    }

    public static long convetUtcTime(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        // 取得时间偏移量：
        int zoneOffset = cal.get(Calendar.ZONE_OFFSET);
        // 3、取得夏令时差：
        int dstOffset = cal.get(Calendar.DST_OFFSET);
        // 4、从本地时间里扣除这些差量，即可以取得UTC时间：
        cal.add(Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        return cal.getTimeInMillis();

    }

    public static long convetLocalTime(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        // 取得时间偏移量：
        int zoneOffset = cal.get(Calendar.ZONE_OFFSET);
        // 3、取得夏令时差：
        int dstOffset = cal.get(Calendar.DST_OFFSET);
        // 4、从本地时间里扣除这些差量，即可以取得UTC时间：
        cal.add(Calendar.MILLISECOND, (zoneOffset + dstOffset));
        return cal.getTimeInMillis();

    }

    /**
     * @param startTime
     * @param endTime
     * @return
     * @Description 获取花了多少时间
     * @author lilq
     * @date 2018年8月25日 下午3:51:08
     */
    public static String getUseTime(long startTime, long endTime) {
        long gapTime = endTime - startTime; // 毫秒
        long secTime = gapTime / 1000; // 秒
        long misTime = gapTime % 1000;
        long misTime1 = misTime % 100;
        long misTime2 = misTime1 % 10;
        String result = secTime + "." + misTime / 100 + misTime1 / 10 + misTime2 + "秒";
        return result;
    }

    /**
     * 将Long类型的timestamp转为LocalDateTime(timestamp为秒)
     *
     * @param timestamp
     * @return
     */
    public static LocalDateTime getDateTimeOfTimestamp(Long timestamp) {
        if (timestamp == null) {
            return null;
        }
        Instant instant = Instant.ofEpochMilli(timestamp * 1000);
        ZoneId zone = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(instant, zone);
    }

    /**
     * 将LocalDateTime转为Long类型的timestamp(返回秒)
     *
     * @param localDateTime
     * @return
     */
    public static Long getTimestampOfDateTime(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDateTime.atZone(zone).toInstant();
        return instant.toEpochMilli() / 1000;
    }

    /**
     * @param time
     * @param format
     * @return
     * @Description 时间戳转换日期格式
     * @author lilq
     * @date 2019年6月21日 上午9:33:39
     */
    public static String timeStamp2Date(Long time, String format) {
        if (time == null) {
            return null;
        }
        if (ObjectUtil.isEmpty(format)) {
            format = FORMAT_0;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(parseDate(time));
    }

    /**
     * 将Date转为Long类型的timestamp(返回秒)
     *
     * @param date
     * @return
     */
    public static Long getTimestampOfDate(Date date) {
        if (date == null) {
            return null;
        }
        return date.getTime() / 1000;
    }

    /**
     * 将LocalDateTime转为Date
     *
     * @param localDateTime
     * @return
     */
    public static Date getDateOfDateTime(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        ZoneId zoneId = ZoneId.systemDefault();
        Date date = Date.from(localDateTime.atZone(zoneId).toInstant());
        return date;
    }

    /**
     * 毫秒转换为秒
     *
     * @param time
     * @return
     */
    public static Long getSecond4Millisecond(Long time) {
        if (time == null) {
            return null;
        }
        String str = String.valueOf(time);
        if (str.length() == 13) {
            return time / 1000;
        }
        return time;
    }

    /**
     * @param dateStr
     * @return
     * @throws Exception
     * @Description 字符串转日期格式
     * @author lilq
     * @date 2019年8月21日 上午10:17:18
     */
    public static final Date parseDate4Str(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_0);
        try {
            return format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static final Date parseDate(Long time) {
        if (time == null) {
            return null;
        }
        if (String.valueOf(time).length() == 10) {
            time = time * 1000;
        }
        return new Date(time);
    }

    public static final Long getTimeOfDate(Date date) {
        if (date == null) {
            return null;
        }
        return date.getTime();
    }

    public static long subDate2Day(Date date) {
        if (date == null) {
            return 0L;
        }
        long time = getSecond4Millisecond(date.getTime());
        long currentTime = getSecond4Millisecond(System.currentTimeMillis());
        long day = (time - currentTime) / (24 * 60 * 60);
        if (day < 0) {
            day = 0;
        }
        return day;
    }

    /**
     * @param frontTime
     * @return
     * @Description: 与当前时间相差几天
     * @date 2019年12月3日下午4:06:14
     * @author LiQing
     */
    public static long subDate2Day(long frontTime) {
        try {
            String time1 = timeStamp2Date(frontTime, FORMAT_1);
            Date date1 = parseDate(time1, FORMAT_1);
            Date date2 = getFormatDate(FORMAT_1);
            LocalDate ld1 = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate ld2 = date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            return ld2.toEpochDay() - ld1.toEpochDay();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    public static LocalDateTime parseStringToDateTime(String time) {
        if (StrUtil.isBlank(time)) {
            return null;
        }
        DateTimeFormatter formatter = null;
        if (time.length() > 10) {
            formatter = new DateTimeFormatterBuilder()
                    .appendPattern(FORMAT_0)
                    .toFormatter();
        } else {
            formatter = new DateTimeFormatterBuilder()
                    .appendPattern(FORMAT_1)
                    .parseDefaulting(ChronoField.HOUR_OF_DAY, 23)
                    .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 59)
                    .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 59)
                    .parseDefaulting(ChronoField.MILLI_OF_SECOND, 0)
                    .toFormatter();
        }
        return LocalDateTime.parse(time, formatter);
    }

    public static LocalDate parseStringToLocalDate(String time) {
        if (StrUtil.isBlank(time)) {
            return null;
        }
        final DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern(FORMAT_1)
                .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .toFormatter();
        return LocalDate.parse(time, formatter);
    }

    public static String formatLocalDateToString(LocalDate time) {
        if (time == null) {
            return null;
        }
        final DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern(FORMAT_1)
                .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .toFormatter();
        return time.format(formatter);
    }

    /**
     * 计算两个时间的天数差
     * @param startDate
     * @param endDate
     * @return
     * @throws ParseException
     */
    public static int getDayDiffer(Date startDate, Date endDate) throws ParseException {
        long startDateTime = getFormatDate(startDate,FORMAT_1).getTime();
        long endDateTime =getFormatDate(endDate,FORMAT_1).getTime();
        return (int) ((endDateTime - startDateTime) / (1000 * 3600 * 24));
    }

    /*
     * 获取最早时间
     * @author lilq
     * @param localDateTimes:
     * @return LocalDateTime
     * @date 2021-01-15 15:21:00
     **/
    public static LocalDateTime getEarliestDate(List<LocalDateTime> localDateTimes) {
        if (CollectionUtil.isEmpty(localDateTimes)) {
            return null;
        }
        // 升序排序
        Collections.sort(localDateTimes, new Comparator<LocalDateTime>() {
            @Override
            public int compare(LocalDateTime date1, LocalDateTime date2) {
                if (date1 == null && date2 != null) {
                    return 1;
                }
                if (date1 != null && date2 == null) {
                    return -1;
                }
                boolean isAfter = DateUtils.isAfter(date1, date2);
                if (isAfter) {
                    return 1;
                }
                return -1;
            }
        });
        return localDateTimes.get(0);
    }

}

