package utils;//


import cn.hutool.core.util.ObjectUtil;

/**
 * 生成器工具
 *
 * @author lilq
 * @date 2020-11-12 08:11:31
 **/
public class GeneratorUtils {

    /**
     * 合同分类编码前缀
     */
    public static final String CONSTRCT_TYPE_CODE_PREFIX = "FL";

    /*
     * 生成合同分类编码（规则：FL001,FL002,FL001002）
     * @author lilq
     * @param contractTypeCode:
     * @param isAddOne:
     * @return String
     * @date 2020-11-13 09:12:14
     **/
    public static String genContractTypeCode(String contractTypeCode, boolean isAddOne) {
        if (ObjectUtil.isEmpty(contractTypeCode)) {
            return CONSTRCT_TYPE_CODE_PREFIX + "001";
        }
        String contractCode = "";
        if (isAddOne) {
            String code = contractTypeCode.split(CONSTRCT_TYPE_CODE_PREFIX)[1];
            String codePrefix = code.substring(0, code.length() - 3);
            String idxStr = code.substring(code.length() - 3, code.length());
            int idx = Integer.valueOf(idxStr) + 1;
            contractCode = CONSTRCT_TYPE_CODE_PREFIX + codePrefix + "00" + idx;
        } else {
            contractCode = contractTypeCode + "001";
        }
        return contractCode;
    }

    /*
     * 生成随机数编码
     * @author lilq
     * @return String
     * @date 2020-11-18 15:20:41
     **/
    public static String genRandomCode() {
        return RandomUtils.createRandomStr();
    }

    /*
     * 生成节点编码
     * @author lilq
     * @return String
     * @date 2020-11-18 15:20:41
     **/
    public static String genNodeCode() {
        return "N" + RandomUtils.createRandomStr();
    }

    /*
     * 生成交付物编码
     * @author lilq
     * @return String
     * @date 2020-11-18 15:20:41
     **/
    public static String genDeliverablesCode() {
        return "D" + RandomUtils.createRandomStr();
    }

    /*
     * 生成节点组编码
     * @author lilq
     * @return String
     * @date 2020-11-18 15:20:41
     **/
    public static String genNodeGroupCode() {
        return "NG" + RandomUtils.createRandomStr();
    }

    /*
     * 生成开票收款编码
     * @author lilq
     * @return String
     * @date 2021-01-13 09:34:22
     **/
    public static String genBillingCollectionCode() {

        return "KS" + RandomUtils.createRandomStr();
    }

    /*
     * 生成回款记录编码
     * @author hualin
     * @return String
     * @date 2021-03-29 17:34:22
     **/
    public static String genPaymentCollectionCode() {
        return "HK" + RandomUtils.createRandomStr();
    }

    /*
     * 生成项目编码
     * @author hualin
     * @return String
     * @date 2021-03-31 16:34:22
     **/
    public static String genProjectCode() {
        return "XM" + RandomUtils.createRandomStr();
    }

    /*
     * 生成工程合同编码
     * @author hualin
     * @return String
     * @date 2021-03-31 16:34:22
     **/
    public static String genEngineerContractCode() {
        return "GCHT" + RandomUtils.createRandomStr();
    }

    /*
     * 生成期区楼栋编码
     * @author hualin
     * @return String
     * @date 2021-03-31 16:34:22
     **/
    public static String genBuildingCode() {
        return "B" + RandomUtils.createRandomStr();
    }

    /*
     * 生成期区楼栋单位工程编码
     * @author hualin
     * @return String
     * @date 2021-03-31 16:34:22
     **/
    public static String genEngineerCode() {
        return "DWGC" + RandomUtils.createShortRandom();
    }

}