package utils;


import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;
import java.util.Random;

/**
 * @Description 随机数工具类
 * @author lilq
 * @date 2019年6月11日 下午4:21:10
 */
public class RandomUtils {

	/**
	 * 生成16位随机数值(当前毫秒后11位+5位伪随机数)
	 * 
	 * @return
	 */
	public static String createRandomStr() {
		String millis = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS");
		Random random = new Random();
		return millis + String.valueOf(random.nextDouble()).substring(2, 7);
	}

	/**
	 * 生成8位随机数值
	 * 
	 * @return
	 */
	public static String createShortRandom() {
		return createRandomStr().substring(8, 16);
	}

	/**
	 * 生成12位长度的20进制
	 * 
	 * @return
	 */
	public static String createRandom3oxStr() {
		return Long.toString(Long.parseLong(createRandomStr()), 20);
	}

	/**
	 * 生成36进制
	 * 
	 * @return
	 */
	public static String createRandom36Str() {
		return Long.toString(Long.parseLong(createRandomStr()), Character.MAX_RADIX);
	}
}
