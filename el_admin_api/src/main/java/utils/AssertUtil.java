package utils;


import com.example.api.exception.CommonRuntimeException;

public class AssertUtil {

    public static void isTrue(Boolean condition,String message){
        if (condition){
            throw new CommonRuntimeException(message);
        }
    }

    public static void isFalse(Boolean condition,String message){
        if (!condition){
            throw new CommonRuntimeException(message);
        }
    }
}
