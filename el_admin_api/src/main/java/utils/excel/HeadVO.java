package utils.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @Author Story
 * @Date 2022/3/26 17:47
 * @Version 1.0
 */
@Data
@Builder
public class HeadVO implements Comparable<HeadVO> {
    /**
     * 列头名
     */
    @ExcelProperty()
    private List<String> headTitle;
    /**
     * 字段名
     */
    private String key;
    /**
     * 主排序
     */
    private int index;
    /**
     * 次排序
     */
    private int order;

    @Override
    public int compareTo(HeadVO o) {
        if (this.index == o.getIndex()) {
            return this.order - o.getOrder();
        }
        return this.index - o.getIndex();
    }
}
